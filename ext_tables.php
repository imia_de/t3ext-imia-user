<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'IMIA.' . $_EXTKEY,
    'user_access',
    'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/Plugin.xlf:user_access'
);
\IMIA\ImiaBaseExt\Utility\ExtensionUtility::registerPluginFlexform($_EXTKEY, 'user_access', 'Configuration/FlexForms/AccessData.xml');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'IMIA.' . $_EXTKEY,
    'user_profile',
    'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/Plugin.xlf:user_profile'
);
\IMIA\ImiaBaseExt\Utility\ExtensionUtility::registerPluginFlexform($_EXTKEY, 'user_profile', 'Configuration/FlexForms/Profile.xml');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'IMIA.' . $_EXTKEY,
    'user_register',
    'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/Plugin.xlf:user_register'
);
\IMIA\ImiaBaseExt\Utility\ExtensionUtility::registerPluginFlexform($_EXTKEY, 'user_register', 'Configuration/FlexForms/Registration.xml');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'IMIA.' . $_EXTKEY,
    'user_delete',
    'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/Plugin.xlf:user_delete'
);
\IMIA\ImiaBaseExt\Utility\ExtensionUtility::registerPluginFlexform($_EXTKEY, 'user_delete', 'Configuration/FlexForms/Deletion.xml');

$GLOBALS['TCA']['tt_content']['ctrl']['requestUpdate'] .= ',settings.email.type,settings.email.admin.type,settings.delete.enabled,settings.delete.page';

$extConf = unserialize($_EXTCONF);
if (TYPO3_MODE === 'BE' && $extConf['backend']) {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'IMIA.' . $_EXTKEY,
        'web',
        'user_backend',
        '',
        [
            'Backend' => 'show, upload, mapping, import',
        ],
        [
            'access' => 'user,group',
            'icon'   => 'EXT:' . $_EXTKEY . '/Resources/Public/Images/Icons/Backend.' .
                (\TYPO3\CMS\Core\Utility\GeneralUtility::compat_version('7.0') ? 'svg' : 'png'),
            'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/ModuleBackend.xlf',
        ]
    );
}

unset($GLOBALS['TCA']['fe_users']['columns']['country']);

$GLOBALS['TCA']['tx_powermail_domain_model_field']['columns']['feuser_value']['config']['items'][] = [
    $GLOBALS['TCA']['fe_users']['columns']['first_name']['label'],
    'first_name'
];
$GLOBALS['TCA']['tx_powermail_domain_model_field']['columns']['feuser_value']['config']['items'][] = [
    $GLOBALS['TCA']['fe_users']['columns']['last_name']['label'],
    'last_name'
];