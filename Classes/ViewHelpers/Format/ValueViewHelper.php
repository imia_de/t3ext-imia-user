<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaUser\ViewHelpers\Format;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_user
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class ValueViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * @var array
     */
    protected $hookObjects;

    /**
     * @param string $table
     * @param string $field
     * @return string
     */
    public function render($table, $field = null)
    {
        $value = $this->renderChildren();
        if (is_object($value)) {
            switch (get_class($value)) {
                case 'TYPO3\CMS\Extbase\Persistence\ObjectStorage':
                case 'TYPO3\CMS\Extbase\Persistence\Generic\LazyObjectStorage':
                    $values = [];
                    foreach ($value as $subvalue) {
                        $values[] = (string)$subvalue;
                    }

                    $output = implode(', ', $values);
                    break;
                case 'DateTime':
                    $output = $value->format('d.m.Y H:i');
                    break;
                default:
                    $output = $value;
            }
        } else {
            $output = $value;
        }

        if ($field && (is_string($output) || is_numeric($output))) {
            $tcaColumnConfig = $GLOBALS['TCA'][$table]['columns'][$field]['config'];
            if (in_array($tcaColumnConfig['type'], ['select', 'radio'])) {
                $values = [];
                if (isset($tcaColumnConfig['items']) && is_array($tcaColumnConfig['items'])) {
                    foreach ($tcaColumnConfig['items'] as $item) {
                        $values[$item[1]] = $this->translate($item[0]);
                    }
                }

                if ($tcaColumnConfig['foreign_table']) {
                    $foreignTCA = $GLOBALS['TCA'][$tcaColumnConfig['foreign_table']]['ctrl'];

                    if (GeneralUtility::_GP('id')) {
                        $TSConfig = BackendUtility::getTCEFORM_TSconfig('pages', BackendUtility::getRecordWSOL('pages', GeneralUtility::_GP('id')));
                        $TSConfig['_THIS_ROW'] = [];
                        $TSConfig['_CURRENT_PID'] = $TSConfig['_THIS_UID'];
                        $TSConfig['_THIS_UID'] = null;
                    } else {
                        $TSConfig = [];
                    }

                    $foreignTableWhere = BackendUtility::replaceMarkersInWhereClause(
                        ($tcaColumnConfig['foreign_table_where']), $table, $field, $TSConfig
                    );

                    $foreignResult = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
                        '*',
                        $tcaColumnConfig['foreign_table'],
                        '1=1 ' . ($foreignTCA['delete'] ? 'AND ' . $foreignTCA['delete'] . ' = 0 ' : '') . $foreignTableWhere
                    );
                    while ($foreignRow = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($foreignResult)) {
                        $label = $foreignRow[$foreignTCA['label']];
                        if ($foreignTCA['label_alt_force']) {
                            $labelAlt = array_map('trim', explode(',', $foreignTCA['label_alt']));
                            $labels = [$label];
                            foreach ($labelAlt as $labelAltField) {
                                $labels[] = $foreignRow[$labelAltField];
                            }
                            $label = implode(', ', $labels);
                        }

                        $values[$foreignRow['uid']] = $label;
                    }
                }

                if (array_key_exists($output, $values)) {
                    $output = $values[$output];
                }
            }
        }

        // Hook: getFieldValue
        foreach ($this->getHookObjects() as $hookObject) {
            if (method_exists($hookObject, 'getFieldValue')) {
                $hookObject->getFieldValue($field, $value, $output, $this);
            }
        }

        return $output;
    }

    /**
     * Translate by key
     *
     * @param string $key
     * @param array $arguments
     * @return string
     */
    protected function translate($key, $arguments = [])
    {
        return (string)\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($key, 'imia_user', $arguments);
    }

    /**
     * @return array
     */
    protected function getHookObjects()
    {
        if (!$this->hookObjects) {
            $this->hookObjects = [];

            if (is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['tx_imiauser']['userBackend'])) {
                foreach ($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['tx_imiauser']['userBackend'] as $_classRef) {
                    $this->hookObjects[] = &\TYPO3\CMS\Core\Utility\GeneralUtility::getUserObj($_classRef);
                }
            }
        }

        return $this->hookObjects;
    }
}