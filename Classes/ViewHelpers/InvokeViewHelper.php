<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaUser\ViewHelpers;

/**
 * @package     imia_user
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class InvokeViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * Invokes a method of an object
     *
     * @param string $method
     * @param array $arguments
     * @param string $params
     * @return string
     */
    public function render($method, $arguments = [], $params = null)
    {
        $object = $this->renderChildren();

        $value = '';
        if (is_object($object) && method_exists($object, $method)) {
            $value = $object->$method($arguments);

            if ($params) {
                foreach (explode('.', $params) as $param) {
                    $paramMethod = 'get' . ucfirst($param);

                    if (method_exists($value, $paramMethod)) {
                        $value = $value->$paramMethod();
                    }
                }
            }
        }

        return $value;
    }
}