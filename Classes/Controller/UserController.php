<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaUser\Controller;

use IMIA\ImiaUser\Domain\Model\Language;
use IMIA\ImiaUser\Domain\Model\User;
use IMIA\ImiaUser\Domain\Model\UserGroup;
use IMIA\ImiaUser\Domain\Repository\CountryRepository;
use IMIA\ImiaUser\Domain\Repository\LanguageRepository;
use IMIA\ImiaUser\Domain\Repository\UserGroupRepository;
use IMIA\ImiaUser\Domain\Repository\UserRepository;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Service\FlexFormService;

/**
 * @package     imia_user
 * @subpackage  Controller
 * @author      David Frerich <d.frerich@imia.de>
 */
class UserController extends AbstractController
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @var array
     */
    protected $fields;

    /**
     * @var array
     */
    protected $registerChecks;

    /**
     * @var User
     */
    protected static $tempUser = null;

    /**
     * @var array
     */
    protected $formErrors = [];

    /**
     * @param ViewInterface $view
     */
    protected function initializeView(ViewInterface $view)
    {
        parent::initializeView($view);

        if (!$this->userRepository) {
            $this->userRepository = $this->objectManager->get(UserRepository::class);
        }
        if (!$this->userGroupRepository) {
            $this->userGroupRepository = $this->objectManager->get(UserGroupRepository::class);
        }
        if (!$this->countryRepository) {
            $this->countryRepository = $this->objectManager->get(CountryRepository::class);
        }
        if (!$this->languageRepository) {
            $this->languageRepository = $this->objectManager->get(LanguageRepository::class);
        }

        if ($this->getSetting('storagePid')) {
            $this->userRepository->setStoragePageIds([(int)$this->getSetting('storagePid')]);
            $this->userGroupRepository->setStoragePageIds([(int)$this->getSetting('storagePid')]);
        }
        $this->userRepository->setRespectStoragePage(true);

        if ($this->getSetting('contentUid')) {
            $contentElement = $this->getDb()->exec_SELECTgetSingleRow('*',
                'tt_content', 'uid = ' . (int)$this->getSetting('contentUid'));

            if (is_string($contentElement['pi_flexform'])) {
                /** @var FlexFormService $flexFormService */
                $flexFormService = $this->objectManager->get(FlexFormService::class);
                if ($contentElement['pi_flexform'] !== '') {
                    $flexFormConfiguration = $flexFormService->convertFlexFormContentToArray($contentElement['pi_flexform']);
                } else {
                    $flexFormConfiguration = [];
                }

                if (isset($flexFormConfiguration['settings']) && is_array($flexFormConfiguration['settings']) && !empty($flexFormConfiguration['settings'])) {
                    $settings = $flexFormConfiguration['settings'];
                    ArrayUtility::mergeRecursiveWithOverrule($settings, $this->settings);
                    $this->settings = $settings;
                    $this->resolvedSettings = null;
                }
            }
        }
    }

    /**
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\InvalidNumberOfConstraintsException
     */
    public function accessAction()
    {
        $user = $this->getUser(true);
        if ($this->getRequest()->getMethod() == 'POST' && count($this->getRequest()->getArguments()) > 0) {
            $this->controllerContext->getFlashMessageQueue()->getAllMessagesAndFlush();
            if ($this->getRequest()->hasArgument('password') && $this->getRequest()->getArgument('password')) {
                if ($this->validatePassword($this->getRequest()->getArgument('password'), $user)) {
                    $user->setPassword($this->getRequest()->getArgument('password'));
                    $this->userRepository->update($user);

                    $this->addSuccess('user_access_form_success_password');
                }
            } elseif ($this->getRequest()->hasArgument('newPassword') && $this->getRequest()->getArgument('newPassword')) {
                if ($this->validatePassword($this->getRequest()->getArgument('newPassword'), $user)) {
                    $user->setPassword($this->getRequest()->getArgument('newPassword'));
                    $this->userRepository->update($user);

                    $this->addSuccess('user_access_form_success_password');
                }
            }

            if ($this->getSetting('emailAsUsername')) {
                if ($this->getRequest()->hasArgument('newEmail') && $this->getRequest()->getArgument('newEmail')) {
                    $newEmail = $this->getRequest()->getArgument('newEmail');

                    if (!GeneralUtility::validEmail($newEmail)) {
                        $this->addError('user_form_error_email_valid', 'new_email');
                        $this->formErrors[] = 'new_email';
                    } else {
                        $otherUser = $this->userRepository->findOneByEmail($newEmail);
                        if ($otherUser && (!$user || $otherUser->getUid() != $user->getUid())) {
                            $this->addError('user_form_error_email_duplicate', 'new_email');
                            $this->formErrors[] = 'new_email';
                        }

                        if (!$otherUser) {
                            $otherUser = $this->userRepository->findOneByUsername($newEmail);
                            if ($otherUser && (!$user || $otherUser->getUid() != $user->getUid())) {
                                $this->addError('user_form_error_email_duplicate', 'new_email');
                                $this->formErrors[] = 'new_email';
                            }
                        }

                        if (!$otherUser) {
                            $user->setEmail($newEmail);
                            $user->setUsername($newEmail);
                            $this->userRepository->update($user);

                            $this->addSuccess('user_access_form_success_email');
                        }
                    }
                }
            } else {
                if ($this->getRequest()->hasArgument('newUsername') && $this->getRequest()->getArgument('newUsername')) {
                    $newUsername = $this->getRequest()->getArgument('newUsername');

                    if ($this->userRepository->findOneByUsername($newUsername)) {
                        $this->addError('user_form_error_username_duplicate', 'new_username');
                        $this->formErrors[] = 'new_username';
                    } elseif (strlen($user->getUsername()) < 4) {
                        $this->addError('user_form_error_username_length', 'new_username');
                        $this->formErrors[] = 'new_username';
                    } else {
                        $user->setUsername($newUsername);
                        $this->userRepository->update($user);

                        $this->addSuccess('user_access_form_success_username');
                    }
                }
            }
        }

        $this->view->assign('passwordPattern', $this->getPasswordPattern());
        $this->view->assign('passwordPatternMinLength', $this->getPasswordPattern('minLength'));
        $this->view->assign('passwordPatternSpecialCharacter', $this->getPasswordPattern('specialCharacter'));
        $this->view->assign('passwordPatternLowerUpperCase', $this->getPasswordPattern('lowerUpperCase'));
        $this->view->assign('passwordPatternNumber', $this->getPasswordPattern('number'));

        $this->view->assign('user', $user);
        $this->view->assign('cobj', $this->configurationManager->getContentObject()->data);
    }

    /**
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     * @throws \Exception
     */
    public function profileAction()
    {
        $user = $this->getUser(true);

        if ($this->getRequest()->getMethod() == 'POST' && count($this->getRequest()->getArguments()) > 0) {
            $this->controllerContext->getFlashMessageQueue()->getAllMessagesAndFlush();

            $this->formErrors = [];
            if ($this->validate($user)) {
                $registerCheckError = false;
                if ($this->getSetting('profileRegisterChecks')) {
                    $registerChecks = $this->getRegisterChecks();

                    if (count($registerChecks) > 0) {
                        foreach ($registerChecks as $registerCheck) {
                            if ($registerCheck['mandatory']) {
                                if (!$this->getRequest()->hasArgument($registerCheck['id']) || !$this->getRequest()->getArgument($registerCheck['id'])) {
                                    $this->formErrors[] = $registerCheck['id'];

                                    if ($this->getSetting('registerCheckErrorAsOne')) {
                                        $registerCheckError = true;
                                    } else {
                                        $this->addError($registerCheck['error'] ?: $this->translate('user_form_error_registercheck') .
                                            $registerCheck['text'], $registerCheck['id']);
                                    }
                                }
                            }
                        }
                    }
                }
                if ($this->getSetting('gtc') && $this->getSetting('gtcEditable')) {
                    if (!$this->getSetting('gtcOptional')) {
                        if (!$user->getGtc()) {
                            if (!$this->getRequest()->hasArgument('gtc') || !$this->getRequest()->getArgument('gtc')) {
                                if (!$this->getRequest()->hasArgument('newgtc') || !$this->getRequest()->getArgument('newgtc')) {
                                    $this->addError('user_form_error_gtc', 'gtc');
                                    $this->formErrors[] = 'gtc';
                                    $this->formErrors[] = 'newgtc';
                                    $registerCheckError = false;
                                }
                            }
                        }
                    } else {
                        if ($this->getRequest()->hasArgument('gtc')) {
                            $user->setGtc((bool)$this->getRequest()->getArgument('gtc'));
                        } elseif ($this->getRequest()->hasArgument('newgtc')) {
                            $user->setGtc((bool)$this->getRequest()->getArgument('newgtc'));
                        }
                    }
                }

                if ($registerCheckError) {
                    $this->addError('user_form_error_gtc', 'gtc');
                }

                if (count($this->formErrors) === 0) {
                    $this->uploadFiles($user);

                    if ($this->getSetting('profileRegisterChecks')) {
                        $registerChecks = $this->getRegisterChecks();

                        $userRegisterChecks = [];
                        if (count($registerChecks) > 0) {
                            foreach ($registerChecks as $registerCheck) {
                                if ($this->getRequest()->hasArgument($registerCheck['id']) && $this->getRequest()->getArgument($registerCheck['id'])) {
                                    $userRegisterChecks[$registerCheck['key']] = $registerCheck['text'];
                                }
                            }
                        }
                        if (count($userRegisterChecks)) {
                            $user->setRegisterChecks($registerChecks);
                        }
                    }

                    $this->userRepository->update($user);
                    $this->clearCache();

                    $this->addSuccess('user_profile_form_success');

                    $userFields = $this->getFields();
                    if (array_key_exists('language', $userFields['enabled'])) {
                        if (($user->getLanguage() && $user->getLanguage()->getUid() != $GLOBALS['TSFE']->sys_language_uid) || $GLOBALS['TSFE']->sys_language_uid > 0) {
                            $hash = '';
                            if ($this->getContentObject()->data['uid']) {
                                $hash = '#c' . $this->getContentObject()->data['uid'];
                            }

                            $this->redirectToUri($this->uriBuilder->setArguments([
                                    'L' => $user->getLanguage() ? $user->getLanguage()->getUid() : 0,
                                ])->build() . $hash
                            );
                        }
                    }
                } else {
                    $this->view->assign('formErrors', array_unique($this->formErrors));
                    $this->view->assign('postArguments', $this->getRequest()->getArguments());
                }
            } else {
                $this->view->assign('formErrors', array_unique($this->formErrors));
                $this->view->assign('postArguments', $this->getRequest()->getArguments());
            }
        }

        if (!$this->getSetting('gtcOptional') && $user->getGtc()) {
            $this->settings['gtcEditable'] = false;
            $this->resolvedSettings['gtcEditable'] = false;
            $this->view->assign('settings', $this->getSettings());
        }

        if ($this->getSetting('profileRegisterChecks')) {
            $this->view->assign('registerChecks', $this->getRegisterChecks());
        }

        $this->view->assign('user', $user);
        $this->view->assign('fields', $this->getFields());
        $this->view->assign('info', $this->getInfo());
        $this->view->assign('cobj', $this->configurationManager->getContentObject()->data);
    }

    /**
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function registerAction()
    {
        $arguments = $this->getRequest()->getArguments();
        if (self::$tempUser) {
            $user = self::$tempUser;
        } else {
            $user = $this->getNewUser();
        }
        if ($this->settings['captcha']['_typoScriptNodeValue']) {
            $captchaError = '';
            if (isset($arguments['captchaError'])) {
                $captchaError = $arguments['captchaError'];
            }

            @require_once(ExtensionManagementUtility::extPath('imia_user') . 'Classes/Lib/recaptchalib.php');
            $this->view->assign('captcha', recaptcha_get_html($this->settings['captcha']['publicKey'], $captchaError, true));
        }

        $this->view->assign('passwordPattern', $this->getPasswordPattern());
        $this->view->assign('passwordPatternMinLength', $this->getPasswordPattern('minLength'));
        $this->view->assign('passwordPatternSpecialCharacter', $this->getPasswordPattern('specialCharacter'));
        $this->view->assign('passwordPatternLowerUpperCase', $this->getPasswordPattern('lowerUpperCase'));
        $this->view->assign('passwordPatternNumber', $this->getPasswordPattern('number'));

        $this->view->assign('user', $user);
        $this->view->assign('fields', $this->getFields());
        $this->view->assign('info', $this->getInfo());
        $this->view->assign('registerChecks', $this->getRegisterChecks());
        $this->view->assign('cobj', $this->configurationManager->getContentObject()->data);
        $this->view->assign('formErrors', isset($arguments['formErrors']) ? array_unique((array)$arguments['formErrors']) : []);

        unset($arguments['captchaError']);
        unset($arguments['formErrors']);
        $this->view->assign('postArguments', $arguments);
        $this->view->assign('contentId', $this->getContentId());
    }

    /**
     * @return int
     */
    protected function getContentId()
    {
        if ($this->getContentObject()->data['uid']) {
            return (int)$this->getContentObject()->data['uid'];
        } elseif ($this->getSetting('userformUid')) {
            return (int)$this->getSetting('userformUid');
        } elseif ($this->getSetting('contentUid')) {
            return (int)$this->getSetting('contentUid');
        } else {
            return 0;
        }
    }

    /**
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     * @throws \Exception
     */
    public function registerSubmitAction()
    {
        if ($this->getRequest()->hasArgument('contentId')) {
            if ($this->getRequest()->getArgument('contentId') != $this->getContentId()) {
                $this->forward('register');
            }
        }

        $user = $this->getNewUser();
        $registerChecks = $this->getRegisterChecks();

        $captchaError = null;
        if ($this->settings['captcha']['_typoScriptNodeValue']) {
            @require_once(ExtensionManagementUtility::extPath('imia_user') . 'Classes/Lib/recaptchalib.php');
        }

        if ($this->getRequest()->getMethod() == 'POST' && count($this->getRequest()->getArguments()) > 0) {
            $this->controllerContext->getFlashMessageQueue()->getAllMessagesAndFlush();
            $this->formErrors = [];
            $validateSuccess = $this->validate($user, true);

            if ($this->settings['captcha']['_typoScriptNodeValue']) {
                $captchaResponse = recaptcha_check_answer($this->settings['captcha']['privateKey'],
                    $_SERVER['HTTP_X_FORWARDED_FOR'] ?: $_SERVER['REMOTE_ADDR'], $_POST['recaptcha_challenge_field'], $_POST['recaptcha_response_field']);

                if (!$captchaResponse->is_valid) {
                    $captchaError = $captchaResponse->error;
                    $this->addError('user_form_error_captcha', 'captcha');
                }
            }

            $registerCheckError = false;
            if (count($registerChecks) > 0) {
                foreach ($registerChecks as $registerCheck) {
                    if ($registerCheck['mandatory']) {
                        if (!$this->getRequest()->hasArgument($registerCheck['id']) || !$this->getRequest()->getArgument($registerCheck['id'])) {
                            $this->formErrors[] = $registerCheck['id'];

                            if ($this->getSetting('registerCheckErrorAsOne')) {
                                $registerCheckError = true;
                            } else {
                                $this->addError($registerCheck['error'] ?: $this->translate('user_form_error_registercheck') .
                                    $registerCheck['text'], $registerCheck['id']);
                            }
                        }
                    }
                }
            }

            if ($this->getSetting('gtc')) {
                if (!$this->getRequest()->hasArgument('gtc') || !$this->getRequest()->getArgument('gtc')) {
                    if (!$this->getSetting('gtcOptional')) {
                        $this->formErrors[] = 'gtc';

                        $this->addError('user_form_error_gtc', 'gtc');
                        $registerCheckError = false;
                    }
                } else {
                    $user->setGtc(true);
                }
            }

            if ($registerCheckError) {
                $this->addError('user_form_error_gtc', 'gtc');
            }

            if ($validateSuccess && !$captchaError && count($this->formErrors) === 0) {
                $existingUser = null;
                if ($this->getSetting('duplicateOverwrite')) {
                    $existingUser = $this->userRepository->findOneByUsername($user->getUsername());
                    if (!$existingUser) {
                        $existingUser = $this->userRepository->findOneByEmail($user->getEmail());
                    }
                }

                if ($existingUser) {
                    $existingUser->setLanguage($this->getCurrentLanguage());
                    if (!$existingUser->getConfirmToken()) {
                        $existingUser->generateConfirmToken();
                    }
                    // $existingUser->setDisable(true);

                    $this->validate($existingUser, true);
                    if ($this->getSetting('gtc') && $this->getRequest()->hasArgument('gtc') && $this->getRequest()->getArgument('gtc')) {
                        $existingUser->setGtc(true);
                    }

                    $user = $existingUser;
                }

                if ($this->getSetting('usergroup')) {
                    /** @var UserGroup $userGroup */
                    $userGroup = $this->userGroupRepository->findByUid((int)$this->getSetting('usergroup'));
                    if ($userGroup) {
                        $user->addUsergroup($userGroup);
                    }
                }

                $userRegisterChecks = [];
                if (count($registerChecks) > 0) {
                    foreach ($registerChecks as $registerCheck) {
                        if ($this->getRequest()->hasArgument($registerCheck['id']) && $this->getRequest()->getArgument($registerCheck['id'])) {
                            $userRegisterChecks[$registerCheck['key']] = $registerCheck['text'];
                        }
                    }
                }
                if (count($userRegisterChecks)) {
                    $user->setRegisterChecks($userRegisterChecks);
                }

                if ($this->getSetting('newRecordPid')) {
                    $user->setPid((int)$this->getSetting('newRecordPid'));
                }

                $this->uploadFiles($user);
                $this->callHook('registrationPreSave', [&$user, &$this]);

                if ($user->getUid()) {
                    $this->userRepository->update($user);
                } else {
                    $this->userRepository->add($user);
                }

                /** @var PersistenceManager $persistenceManager */
                $persistenceManager = $this->objectManager->get(PersistenceManager::class);

                $persistenceManager->persistAll();
                $this->callHook('registrationComplete', [&$user, &$this]);

                $this->userRepository->update($user);
                $persistenceManager->persistAll();

                $this->clearCache();
                $this->sendRegistrationMail($user, 'preConfirm');
                $this->updateUserRecord($user);

                if ($this->getSetting('registerSuccess')) {
                    $this->forward('registerSuccess', null, null, ['user' => $user]);
                }
            }
        }

        self::$tempUser = $user;
        $this->forward('register', null, null,
            array_merge($this->getRequest()->getArguments(), ['captchaError' => $captchaError, 'formErrors' => $this->formErrors]));
    }

    /**
     * @param string $token
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     * @throws \Exception
     */
    public function confirmAction($token)
    {
        $user = $this->userRepository->findOneByConfirmToken($token);
        $this->callHook('confirmInit', [&$user, &$this]);

        if ($user) {
            $confirmComplete = false;

            if (!$this->getSetting('activation')) {
                if ($user->getDisable()) {
                    $this->sendRegistrationMail($user, 'confirmed', null, false);
                    $confirmComplete = true;
                }

                $user->setDisable(false);
                $user->clearActivateToken();

                $this->userRepository->update($user);
                $this->objectManager->get(PersistenceManager::class)->persistAll();

                $this->loginUser($user->getUsername(), $user->getPlainPassword(), true, $user->getUid());

                $this->updateUserRecord($user);
            } else {
                if (!$user->getActivateToken()) {
                    $user->setDisable(true);
                    $user->generateActivateToken();
                    $this->sendRegistrationMail($user, 'confirmPreActivation');
                    $this->sendRegistrationMail($user, 'new', 'admin');
                    $confirmComplete = true;
                }

                $this->updateUserRecord($user);
            }

            if ($confirmComplete) {
                $this->callHook('confirmComplete', [&$user, &$this]);
            }

            $this->userRepository->update($user);
            if ($this->getSetting('confirmSuccess')) {
                $this->forward('confirmSuccess', null, null, ['user' => $user]);
            }
        } else {
            $this->addError($this->getSetting('text.confirmError') ?: $this->translate('user_confirm_form_error'));
            $this->forward('register');
        }
    }

    /**
     * @param string $token
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     * @throws \Exception
     */
    public function activateAction($token)
    {
        if ($this->getSetting('activation')) {
            $user = $this->userRepository->findOneByActivateToken($token);

            if ($user) {
                $user->setDisable(false);
                $user->clearActivateToken();
                $this->userRepository->update($user);

                $this->sendRegistrationMail($user, 'confirmActivation');

                $this->updateUserRecord($user);

                if ($this->getSetting('activateSuccess')) {
                    $this->forward('activateSuccess', null, null, ['user' => $user]);
                }
            }
        }
    }

    /**
     * @param User $user
     */
    public function registerSuccessAction(User $user)
    {
        $this->view->assign('user', $user);
        $this->view->assign('cobj', $this->configurationManager->getContentObject()->data);
    }

    /**
     * @param User $user
     */
    public function confirmSuccessAction(User $user)
    {
        $this->view->assign('user', $user);
        $this->view->assign('cobj', $this->configurationManager->getContentObject()->data);
    }

    /**
     * @param User $user
     */
    public function activateSuccessAction(User $user)
    {
        $this->view->assign('user', $user);
        $this->view->assign('cobj', $this->configurationManager->getContentObject()->data);
    }

    /**
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    public function deleteAction()
    {
        $user = $this->getUser(true);

        if ($this->getRequest()->getMethod() == 'POST' && count($this->getRequest()->getArguments()) > 0) {
            $this->controllerContext->getFlashMessageQueue()->getAllMessagesAndFlush();
            if (is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmapClass'])) {
                $this->initializeTceMain();
                $recordWasDeleted = false;

                foreach ($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmapClass'] as $classRef) {
                    $hookObj = GeneralUtility::getUserObj($classRef);
                    if (method_exists($hookObj, 'processCmdmap_deleteAction')) {
                        $hookObj->processCmdmap_deleteAction(
                            $this->userRepository->getTableName(),
                            $user->getUid(),
                            BackendUtility::getRecord($this->userRepository->getTableName(), $user->getUid()),
                            $recordWasDeleted,
                            $this->tce
                        );
                    }
                }
            }
            $this->userRepository->remove($user);

            if ($this->getSetting('deleteSuccess')) {
                $this->forward('deleteSuccess', null, null, ['user' => $user]);
            }
        }

        $this->view->assign('user', $user);
        $this->view->assign('cobj', $this->configurationManager->getContentObject()->data);
    }

    /**
     * @param User $user
     */
    public function deleteSuccessAction(User $user)
    {
        $this->view->assign('user', $user);
        $this->view->assign('cobj', $this->configurationManager->getContentObject()->data);
    }

    /**
     * @param string $type
     * @return string
     */
    protected function getPasswordPattern($type = 'all')
    {
        $passwordPattern = '^';
        if (in_array($type, ['all', 'specialCharacter'])) {
            switch ($this->getSetting('passwordValidate.specialCharacter')) {
                case 2:
                    $passwordPattern .= '(?=.*[^#+€§%~!$@!%&*?])';
                    break;
                case 1:
                    $passwordPattern .= '(?=.*[#+€§%~!$@!%&*?])';
                    break;
                default:
                    break;
            }
        }
        if (in_array($type, ['all', 'lowerUpperCase'])) {
            switch ($this->getSetting('passwordValidate.lowerUpperCase')) {
                case 3:
                    $passwordPattern .= '(?=.*[^a-z])';
                    break;
                case 2:
                    $passwordPattern .= '(?=.*[^A-Z])';
                    break;
                case 1:
                    $passwordPattern .= '(?=.*[a-z])(?=.*[A-Z])';
                    break;
                default:
                    break;
            }
        }
        if (in_array($type, ['all', 'number'])) {
            switch ($this->getSetting('passwordValidate.number')) {
                case 2:
                    $passwordPattern .= '(?=.*[^0-9])';
                    break;
                case 1:
                    $passwordPattern .= '(?=.*[0-9])';
                    break;
                default:
                    break;
            }
        }
        if (in_array($type, ['all', 'minLength'])) {
            $passwordPattern .= '.{' . (int)(int)$this->getSetting('passwordValidate.minLength') . ',}$';
        } else {
            $passwordPattern .= '.*$';
        }

        return $passwordPattern;
    }

    /**
     * @return User
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    protected function getNewUser()
    {
        /** @var User $user */
        $user = GeneralUtility::makeInstance(User::class);
        $user->generateConfirmToken();
        $user->setLanguage($this->getCurrentLanguage());
        $user->setDisable(true);

        $this->callHook('registrationInit', [&$user, &$this]);

        return $user;
    }

    /**
     * @return array
     */
    protected function getRegisterChecks()
    {
        if (!$this->registerChecks) {
            $registerChecks = $this->getSetting('addRegisterCheck');
            if (!$registerChecks || !is_array($registerChecks)) {
                $registerChecks = [];
            }

            $checkCount = 0;
            $this->registerChecks = [];
            foreach ($registerChecks as $key => $registerCheck) {
                $this->registerChecks[$key]['id'] = 'addcheck-' . ++$checkCount;
                $this->registerChecks[$key]['text'] = is_array($registerCheck['text'])
                    ? (isset($registerCheck['text'][$GLOBALS['TSFE']->lang]) ? $registerCheck['text'][$GLOBALS['TSFE']->lang] :
                        ($this->translate($registerCheck['text']['_typoScriptNodeValue']) ?: $registerCheck['text']['_typoScriptNodeValue']))
                    : ($this->translate($registerCheck['text']) ?: $registerCheck['text']);
                $this->registerChecks[$key]['key'] = $key;
                $this->registerChecks[$key]['mandatory'] = $registerCheck['mandatory'];
            }
        }
        $this->callHook('registerChecks', [&$this->registerChecks, &$this]);

        return $this->registerChecks;
    }

    /**
     * @param User $user
     * @param boolean $registration
     * @return boolean
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\InvalidNumberOfConstraintsException
     */
    protected function validate(User &$user, $registration = false)
    {
        $this->callHook('preValidate', [&$user, $registration, &$this]);

        $fields = $this->getFields();
        if ($registration) {
            array_unshift($fields['enabled'], 'password');
            array_unshift($fields['mandatory'], 'password');

            if ($this->getSetting('emailAsUsername')) {
                array_unshift($fields['enabled'], 'email');
                array_unshift($fields['mandatory'], 'email');
            } else {
                array_unshift($fields['enabled'], 'username');
                array_unshift($fields['mandatory'], 'username');
            }
        }
        $fields['enabled'] = array_unique($fields['enabled']);
        $fields['mandatory'] = array_unique($fields['mandatory']);

        foreach ($fields['enabled'] as $field) {
            $setter = 'set' . ucfirst($field);
            $getter = 'get' . ucfirst($field);

            switch ($field) {
                case 'username':
                    if ($this->getRequest()->hasArgument($field)) {
                        $user->$setter($this->getRequest()->getArgument($field));
                    }

                    if (!$user->$getter()) {
                        $this->addError('user_form_error_' . strtolower($field), strtolower($field));
                        $this->formErrors[] = $field;
                    } elseif ($this->userRepository->findOneByUsername($this->getRequest()->getArgument($field)) && !$this->getSetting('duplicateOverwrite')) {
                        $this->addError('user_form_error_' . strtolower($field) . '_duplicate', strtolower($field));
                        $this->formErrors[] = $field;
                    } elseif (strlen($user->$getter()) < 4) {
                        $this->addError('user_form_error_username_length', 'username');
                        $this->formErrors[] = $field;
                    }
                    break;
                case 'email':
                    if ($this->getRequest()->hasArgument($field)) {
                        $user->$setter($this->getRequest()->getArgument($field));
                    }

                    $otherUser = null;
                    if ((in_array($field, $fields['mandatory']) || $this->getSetting('emailAsUsername')) && !$user->$getter()) {
                        $this->addError('user_form_error_' . strtolower($field), strtolower($field));
                        $this->formErrors[] = $field;
                    } elseif (!GeneralUtility::validEmail($user->$getter())) {
                        $this->addError('user_form_error_' . strtolower($field) . '_valid', strtolower($field));
                        $this->formErrors[] = $field;
                    } else {
                        $otherUser = $this->userRepository->findOneByEmail($user->$getter());

                        if ($otherUser && (!$user || $otherUser->getUid() != $user->getUid()) && !$this->getSetting('duplicateOverwrite')) {
                            $this->addError('user_form_error_' . strtolower($field) . '_duplicate', strtolower($field));
                            $this->formErrors[] = $field;
                        }
                    }

                    if ($this->getSetting('emailAsUsername')) {
                        $usernameSetter = 'setUsername';
                        $user->$usernameSetter($user->$getter());

                        if (!$otherUser && $this->getRequest()->hasArgument($field)) {
                            $otherUser = $this->userRepository->findOneByUsername($this->getRequest()->getArgument($field));
                            if ($otherUser && (!$user || $otherUser->getUid() != $user->getUid()) && !$this->getSetting('duplicateOverwrite')) {
                                $this->addError('user_form_error_' . strtolower($field) . '_duplicate', strtolower($field));
                                $this->formErrors[] = $field;
                            }
                        }
                    }

                    break;
                case 'country':
                    $country = null;
                    if ($this->getRequest()->hasArgument($field)) {
                        $country = $this->countryRepository->findByUid((int)$this->getRequest()->getArgument($field));
                    }
                    $user->$setter($country);

                    if (in_array($field, $fields['mandatory']) && !$user->$getter()) {
                        $this->addError('user_form_error_' . strtolower($field), strtolower($field));
                        $this->formErrors[] = $field;
                    }
                    break;
                case 'language':
                    $language = null;
                    if ($this->getRequest()->hasArgument($field)) {
                        $language = $this->languageRepository->findByUid((int)$this->getRequest()->getArgument($field));
                    }
                    $user->$setter($language);

                    if (in_array($field, $fields['mandatory']) && !$user->$getter()) {
                        $this->addError('user_form_error_' . strtolower($field), strtolower($field));
                        $this->formErrors[] = $field;
                    }
                    break;
                case 'image':
                    $fileData = $this->getFileArgument($field);
                    if ($fileData) {
                        if (strpos($fileData['type'], 'image') === false) {
                            $this->addError('user_form_error_' . strtolower($field) . '_valid', strtolower($field));
                            $this->formErrors[] = $field;
                        } elseif ($fileData['size'] / 1024 > 2 * 1024) {
                            $this->addError('user_form_error_' . strtolower($field) . '_size', strtolower($field));
                            $this->formErrors[] = $field;
                        }
                    } elseif (in_array($field, $fields['mandatory'])) {
                        $this->addError('user_form_error_' . strtolower($field), strtolower($field));
                        $this->formErrors[] = $field;
                    }
                    break;
                case 'pdf':
                    $fileData = $this->getFileArgument($field);
                    if ($fileData) {
                        if (strpos($fileData['type'], 'pdf') === false) {
                            $this->addError('user_form_error_' . strtolower($field) . '_valid', strtolower($field));
                            $this->formErrors[] = $field;
                        } elseif ($fileData['size'] / 1024 > 5 * 1024) {
                            $this->addError('user_form_error_' . strtolower($field) . '_size', strtolower($field));
                            $this->formErrors[] = $field;
                        }
                    } elseif (in_array($field, $fields['mandatory'])) {
                        $this->addError('user_form_error_' . strtolower($field), strtolower($field));
                        $this->formErrors[] = $field;
                    }
                    break;
                case 'password':
                    break;
                default:
                    if ($this->getRequest()->hasArgument($field)) {
                        $user->$setter($this->getRequest()->getArgument($field));
                    }

                    if (in_array($field, $fields['mandatory']) && !$user->$getter()) {
                        $this->addError('user_form_error_' . strtolower($field), strtolower($field));
                        $this->formErrors[] = $field;
                    }
                    break;
            }
        }

        if ($registration) {
            if ($this->getSetting('passwordGenerate')) {
                $user->setPassword(substr(str_shuffle(sha1(strtolower(sha1(rand() . time() . 'imiauser' . uniqid())))), 0, 16));
            } else {
                if ($this->getRequest()->hasArgument('password')) {
                    $user->setPassword($this->getRequest()->getArgument('password'));
                }

                $this->validatePassword($user->getPlainPassword(), $user);
            }
        }

        $this->callHook('postValidate', [&$user, $registration, &$this]);

        return count($this->controllerContext->getFlashMessageQueue()->getAllMessages()) == 0 ? true : false;
    }

    /**
     * @param string $password
     * @param User $user
     * @return boolean
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    protected function validatePassword($password, $user)
    {
        $valid = true;

        if (!$password) {
            $this->addError('user_form_error_password', 'password');
            $this->formErrors[] = 'password';
            $valid = false;
        } else {
            if (strlen($password) < (int)$this->getSetting('passwordValidate.minLength')) {
                $this->addError('user_form_error_password_length', 'password', [(int)$this->getSetting('passwordValidate.minLength')]);
                $this->formErrors[] = 'password';
                $valid = false;
            }

            switch ($this->getSetting('passwordValidate.specialCharacter')) {
                case 2:
                    if (!preg_match('/^(?=.*[^#+€§%~!$@!%&*?]).*$/sm', $password)) {
                        $this->addError('user_form_error_password_specialcharacter_2', 'password');
                        $this->formErrors[] = 'password';
                        $valid = false;
                    }
                    break;
                case 1:
                    if (!preg_match('/^(?=.*[#+€§%~!$@!%&*?]).*$/sm', $password)) {
                        $this->addError('user_form_error_password_specialcharacter_1', 'password');
                        $this->formErrors[] = 'password';
                        $valid = false;
                    }
                    break;
                default:
                    break;
            }

            switch ($this->getSetting('passwordValidate.lowerUpperCase')) {
                case 3:
                    if (!preg_match('/^(?=.*[^a-z]).*$/sm', $password)) {
                        $this->addError('user_form_error_password_loweruppercase_3', 'password');
                        $this->formErrors[] = 'password';
                        $valid = false;
                    }
                    break;
                case 2:
                    if (!preg_match('/^(?=.*[^A-Z]).*$/sm', $password)) {
                        $this->addError('user_form_error_password_loweruppercase_2', 'password');
                        $this->formErrors[] = 'password';
                        $valid = false;
                    }
                    break;
                case 1:
                    if (!preg_match('/^(?=.*[a-z])(?=.*[A-Z]).*$/sm', $password)) {
                        $this->addError('user_form_error_password_loweruppercase_1', 'password');
                        $this->formErrors[] = 'password';
                        $valid = false;
                    }
                    break;
                default:
                    break;
            }

            switch ($this->getSetting('passwordValidate.number')) {
                case 2:
                    if (!preg_match('/^(?=.*[^0-9]).*$/sm', $password)) {
                        $this->addError('user_form_error_password_number_2', 'password');
                        $this->formErrors[] = 'password';
                        $valid = false;
                    }
                    break;
                case 1:
                    if (!preg_match('/^(?=.*[0-9]).*$/sm', $password)) {
                        $this->addError('user_form_error_password_number_1', 'password');
                        $this->formErrors[] = 'password';
                        $valid = false;
                    }
                    break;
                default:
                    break;
            }

            if ($this->getSetting('passwordValidate.compareFields')) {
                $compareError = false;
                foreach (array_map('trim', explode(',', $this->getSetting('passwordValidate.compareFields'))) as $compareField) {
                    $getter = 'get' . ucfirst($compareField);
                    if (method_exists($user, $getter)) {
                        $compareValue = $user->$getter();
                        if (strlen($compareValue) > 4 && stripos($password, $compareValue) !== false) {
                            $compareError = true;
                            break;
                        }
                    }
                }

                if ($compareError) {
                    $this->addError('user_form_error_password_invalid', 'password');
                    $this->formErrors[] = 'password';
                }
            }

            if ($this->getSetting('passwordRepeat')) {
                if ($this->getRequest()->hasArgument('passwordRepeat')) {
                    if ($password != $this->getRequest()->getArgument('passwordRepeat')) {
                        $this->addError('user_form_error_password_repeat', 'password_repeat');
                        $this->formErrors[] = 'password_repeat';
                        $valid = false;
                    }
                } elseif ($this->getRequest()->hasArgument('newPasswordRepeat')) {
                    if ($password != $this->getRequest()->getArgument('newPasswordRepeat')) {
                        $this->addError('user_form_error_password_repeat', 'password_repeat');
                        $this->formErrors[] = 'password_repeat';
                        $valid = false;
                    }
                } else {
                    $this->addError('user_form_error_password_repeat', 'password_repeat');
                    $this->formErrors[] = 'password_repeat';
                    $valid = false;
                }
            }
        }

        return $valid;
    }

    /**
     * @param User $user
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    protected function uploadFiles(User &$user)
    {
        $fields = $this->getFields();
        $uploadFields = ['image', 'pdf'];
        foreach ($uploadFields as $field) {
            if (in_array($field, $fields['enabled'])) {
                if ($this->getRequest()->hasArgument($field . '_delete') && $this->getRequest()->getArgument($field . '_delete')) {
                    $deleteMethod = 'remove' . ucfirst($field);
                    $user->$deleteMethod();
                } else {
                    $fileData = $this->getFileArgument($field);
                    if ($fileData) {
                        $setMethod = 'set' . ucfirst($field);
                        $folder = 'community/user/' . $user->getUid() . '_' . $user->getLastName() ?: $user->getCompany() . '/';
                        $file = $this->uploadFile($fileData, (int)$this->getSetting('file.storageUid'), $folder);
                        $user->$setMethod($file);
                    }
                }
            }
        }
    }

    /**
     * @param User $user
     * @param string $mail
     * @param string $mailType
     * @param bool $exception
     * @throws \Exception
     */
    protected function sendRegistrationMail($user, $mail, $mailType = null, $exception = true)
    {
        if ($mailType) {
            $mailSettings = $this->getSetting('email.' . $mailType);
        } else {
            $mailSettings = $this->getSetting('email');
        }

        if ($mailSettings['recipientMail']) {
            $recipient = explode(',', preg_replace('/\s+/', '', $mailSettings['recipientMail']));
        } else {
            $recipient = [$user->getEmail()];
        }

        $emailFormat = $mailSettings['type'];
        if (!in_array($emailFormat, ['plain', 'html'])) {
            $emailFormat = 'plain';
        }

        if ($this->getSetting('overwrite.senderMail')) {
            if ($this->getSetting('overwrite.senderName')) {
                $sender = [$this->getSetting('overwrite.senderMail') => $this->getSettings('overwrite.senderName')];
            } else {
                if ($mailSettings['senderName']) {
                    $sender = [$this->getSetting('overwrite.senderMail') => $mailSettings['senderName']];
                } else {
                    $sender = [$this->getSetting('overwrite.senderMail')];
                }
            }
        } else {
            if ($mailSettings['senderName']) {
                $sender = [$mailSettings['senderMail'] => $mailSettings['senderName']];
            } else {
                $sender = [$mailSettings['senderMail']];
            }
        }

        $templatePath = PATH_site . 'typo3temp/Cache/Code/cache_core/imia_user/' .
            $this->configurationManager->getContentObject()->data['uid'] . '_' . $mail . '_' . ($mailType ? $mailType . '_' : '') .
            $emailFormat . '.txt';

        if (!is_dir(dirname($templatePath))) {
            @mkdir(dirname($templatePath), 0777, true);
            @exec('chmod 777 -R ' . PATH_site . 'typo3temp');
        }

        $templateContent = $mailSettings[$mail . 'Text'][$emailFormat];
        @file_put_contents($templatePath, $templateContent);

        if (file_exists($templatePath)) {
            if ($mailSettings[$mail . 'Subject']) {
                $body = $this->renderTemplate($templatePath, [
                    'user'          => $user,
                    'loginUrl'      => $this->getLoginUrl($user, $mailType),
                    'confirmUrl'    => $this->getConfirmUrl($user, $mailType),
                    'activationUrl' => $this->getActivationUrl($user, $mailType),
                ]);
                if ($user->getPlainPassword() && $this->getSetting('passwordGenerateForceInMail')
                    && $this->getSetting('passwordGenerate') && strpos($body, $user->getPlainPassword()) === false) {
                    $body .= ($emailFormat == 'plain' ? "\n\n" : '<br><br>') . $this->translate('user_form_password') .
                        ': ' . $user->getPlainPassword();
                }

                $this->sendMail(
                    $recipient,
                    $sender,
                    $mailSettings[$mail . 'Subject'],
                    $body,
                    $emailFormat
                );
            } elseif (!$mailType) {
                if ($exception) {
                    throw new \Exception('Registration email could not been sent: No subject for ' . $mail);
                }
            }
        } else {
            if ($exception) {
                throw new \Exception('Registration email could not been sent: Template-file not found');
            }
        }
    }

    /**
     * @param User $user
     * @param string $mailType
     * @return string
     */
    protected function getLoginUrl($user, $mailType = null)
    {
        return !$user->getDisable() && $mailType != 'admin' && $this->getSetting('pages.loginUid')
            ? $this->uriBuilder->reset()->setCreateAbsoluteUri(true)->setTargetPageUid($this->getSetting('pages.loginUid'))->uriFor()
            : '';
    }

    /**
     * @param User $user
     * @param string $mailType
     * @return string
     */
    protected function getConfirmUrl($user, $mailType)
    {
        if (!$user->getConfirmed()) {
            $confirmUrl = $this->uriBuilder->reset()
                ->setCreateAbsoluteUri(true)->setTargetPageUid($GLOBALS['TSFE']->id)
                ->uriFor('confirm', ['token' => $user->getConfirmToken()]);

            return $confirmUrl;
        } else {
            return '';
        }
    }

    /**
     * @param User $user
     * @param string $mailType
     * @return string
     */
    protected function getActivationUrl($user, $mailType)
    {
        return $user->getDisable() && $mailType == 'admin'
            ? $this->uriBuilder->reset()->setCreateAbsoluteUri(true)->setTargetPageUid($GLOBALS['TSFE']->id)
                ->uriFor('activate', ['token' => $user->getActivateToken()])
            : '';
    }

    protected function clearCache()
    {
        if ($this->getSetting('cache.pids')) {
            $pageUids = array_map('intval', explode(',', trim($this->getSetting('cache.pids'))));
            $this->cacheService->clearPageCache($pageUids);
        }
    }

    /**
     * @return array
     */
    protected function getInfo()
    {
        $countries = [''];
        foreach ($this->countryRepository->findAll() as $country) {
            $countries[$country->getUid()] = (string)$country;
        }
        asort($countries);

        $languages = [0 => $this->getSetting('defaultLanguageLabel') ?: 'Standard'];
        foreach ($this->languageRepository->findAll() as $language) {
            $languages[$language->getUid()] = (string)$language;
        }
        asort($languages);

        return [
            'countries' => $countries,
            'languages' => $languages,
        ];
    }

    /**
     * @return \IMIA\ImiaUser\Domain\Model\Language
     */
    protected function getCurrentLanguage()
    {
        /** @var Language $language */
        $language = $this->languageRepository->findByUid($GLOBALS['TSFE']->sys_language_uid);

        return $language;
    }

    /**
     * @return array
     */
    protected function getFields()
    {
        if (!$this->fields) {
            $enabledFieldsConfig = array_map('trim', explode(',', $this->getSetting('fields.enabled')));
            $mandatoryFieldsConfig = array_map('trim', explode(',', $this->getSetting('fields.mandatory')));

            foreach ($enabledFieldsConfig as $key => $field) {
                $enabledFieldsConfig[$key] = GeneralUtility::underscoredToLowerCamelCase(GeneralUtility::camelCaseToLowerCaseUnderscored($field));
            }
            foreach ($mandatoryFieldsConfig as $key => $field) {
                $mandatoryFieldsConfig[$key] = GeneralUtility::underscoredToLowerCamelCase(GeneralUtility::camelCaseToLowerCaseUnderscored($field));
            }

            $fields = ['gender', 'title', 'firstName', 'lastName', 'company', 'position', 'address', 'zip',
                'city', 'country', 'language', 'email', 'telephone', 'mobile', 'fax', 'description', 'image', 'pdf'];

            $this->callHook('getFields', [&$fields, &$this]);

            if ($this->getSetting('emailAsUsername')) {
                $fields = array_diff($fields, ['email']);
            }

            $enabledFields = $mandatoryFields = [];
            foreach ($fields as $field) {
                if (in_array($field, $enabledFieldsConfig)) {
                    $enabledFields[$field] = $field;
                }
                if (in_array($field, $mandatoryFieldsConfig)) {
                    $mandatoryFields[$field] = $field;
                }
            }

            $this->fields = [
                'enabled'   => $enabledFields,
                'mandatory' => $mandatoryFields,
            ];
        }

        return $this->fields;
    }

    /**
     * @param string $username
     * @param string $password
     * @param boolean $force
     * @param integer $uid
     * @return boolean
     */
    protected function loginUser($username, $password, $force = false, $uid = null)
    {
        $check = false;
        $loginData = [
            'username'    => $username,
            'uident_text' => $password,
            'status'      => 'login',
        ];

        $GLOBALS['TSFE']->fe_user->checkPid = '';
        $info = $GLOBALS['TSFE']->fe_user->getAuthInfoArray();
        if ($uid) {
            $user = $this->getDb()->exec_SELECTgetSingleRow('*', 'fe_users', 'uid = ' . $uid .
                $this->getContentObject()->enableFields('fe_users'));
        } else {
            $user = $GLOBALS['TSFE']->fe_user->fetchUserRecord($info['db_user'], $loginData['username']);
        }

        if (isset($user) && $user != '') {
            if (ExtensionManagementUtility::isLoaded('saltedpasswords')) {
                $saltedpasswordsService = GeneralUtility::makeInstance(
                    \TYPO3\CMS\Saltedpasswords\SaltedPasswordService::class);
                $ok = $saltedpasswordsService->compareUident($user, $loginData);
            } else {
                $ok = $GLOBALS['TSFE']->fe_user->compareUident($user, $loginData);
            }

            if ($ok || $force) {
                if ($this->getSetting('login.permantent')) {
                    $user['ses_permanent'] = true;
                } else {
                    $user['ses_permanent'] = false;
                }

                $GLOBALS['TSFE']->fe_user->createUserSession($user);
                $GLOBALS['TSFE']->fe_user->user = $user;
                $GLOBALS['TSFE']->fe_user->setAndSaveSessionData('dummy', true);
                $GLOBALS['TSFE']->fe_user->fetchGroupData();
                $GLOBALS['TSFE']->gr_list = '0,-2';
                $gr_array = array_unique($GLOBALS['TSFE']->fe_user->groupData['uid']);
                $gr_array = array_unique($gr_array);
                // sort
                sort($gr_array);
                if (!empty($gr_array)) {
                    $GLOBALS['TSFE']->gr_list .= ',' . implode(',', $gr_array);
                }

                $check = true;
                $this->user = null;
            } else {
                $check = false;
            }
        }

        return $check;
    }

    /**
     * @param boolean $required
     * @return User
     */
    protected function getUser($required = false)
    {
        if ($this->user === null) {
            $this->user = false;
            if ($GLOBALS['TSFE']->fe_user && $GLOBALS['TSFE']->fe_user->user['uid']) {
                $userUid = (int)$GLOBALS['TSFE']->fe_user->user['uid'];
                $this->user = $this->userRepository->findByUid($userUid);
            }

            if ($required && !$this->user) {
                $GLOBALS['TSFE']->pageNotFoundAndExit('');
            }
        }

        return $this->user;
    }

    /**
     * @param User $user
     */
    protected function updateUserRecord($user)
    {
        $configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
        $cObj = $this->configurationManager->getContentObject();

        $recData = [];
        $recData[$this->getTable()][$user->getUid()]['disable'] = $user->getDisable() ? 1 : 0;
        $this->initializeTceMain();
        $this->tce->start($recData, []);
        $this->tce->process_datamap();

        $this->configurationManager->setContentObject($cObj);
        $this->configurationManager->setConfiguration($configuration);
    }

    /**
     * @return BackendUserAuthentication
     */
    protected function getBeUser()
    {
        return $GLOBALS['BE_USER'];
    }
}