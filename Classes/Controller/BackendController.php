<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaUser\Controller;

use IMIA\ImiaBaseExt\Utility\BackendTypoScript;
use IMIA\ImiaBaseExt\Utility\Session\Storage\SessionStorage;
use IMIA\ImiaUser\Domain\Model\Country;
use IMIA\ImiaUser\Domain\Model\User;
use IMIA\ImiaUser\Domain\Model\UserGroup;
use IMIA\ImiaUser\Utility\Cache;
use PHPExcel_IOFactory;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * @package     imia_user
 * @subpackage  Controller
 * @author      David Frerich <d.frerich@imia.de>
 */
class BackendController extends AbstractController
{
    /**
     * @var array
     */
    protected $settings;

    /**
     * @var array
     */
    protected $pageData;

    /**
     * @var array
     */
    protected $countries;

    /**
     * @var array
     */
    protected $specialMapping;

    /**
     * @var array
     */
    protected $specialMappingValue;

    /**
     * @var array
     */
    protected $usernames = [];

    /**
     * @var array
     */
    protected $userFields;

    /**
     * @var SessionStorage
     */
    protected $sessionStorage;

    /**
     * @var array
     */
    protected $usergroups = [];

    /**
     * @param SessionStorage $sessionStorage
     */
    public function injectSessionStorage(SessionStorage $sessionStorage)
    {
        $this->sessionStorage = $sessionStorage;
    }

    public function showAction()
    {
        $this->controllerContext->getFlashMessageQueue()->getAllMessagesAndFlush();
        $pageData = $this->getPageData();

        if (!in_array($pageData['doktype'], [254, 199])) {
            $this->addFlashMessage($this->translate('be_backend_module'), null, FlashMessage::INFO);
        } else {
            $pageData['module'] = 'fe_users';
        }

        $userFields = $this->getUserFields();
        $userDisplayFields = [];
        foreach ($userFields as $userField) {
            $userDisplayFields[] = $userField;
        }
        $displayFields = [
            'fe_users' => $userDisplayFields,
        ];

        $this->view->assign('userFields', $userFields);
        $this->view->assign('table', $this->getTable());
        $this->view->assign('displayFields', $displayFields);
        $this->view->assign('pageData', $pageData);
    }

    public function uploadAction()
    {
        $this->controllerContext->getFlashMessageQueue()->getAllMessagesAndFlush();
        $pageData = $this->getPageData();

        if (!in_array($pageData['doktype'], [254, 199])) {
            $this->redirect('show');
        } else {
            $pageData['module'] = 'fe_users';
            $this->view->assign('pageData', $pageData);
        }
    }

    public function mappingAction()
    {
        $this->controllerContext->getFlashMessageQueue()->getAllMessagesAndFlush();
        $pageData = $this->getPageData();

        if (!in_array($pageData['doktype'], [254, 199])) {
            $this->redirect('show');
        } else {
            $pageData['module'] = 'fe_users';
            $importData = $this->getImportData($fileName);
            $fields = $this->getFields($importData);

            if (!count($fields)) {
                $this->addError('No data found');
            } else {
                $userFields = $this->getUserFields(true);

                $mapping = $this->getMapping($fields, $userFields);
                $users = $this->getImportedUsers($fields, $userFields, $importData);

                $importPreview = [];
                foreach ($users as $row) {
                    if (count($importPreview) < 5) {
                        $user = $this->createUser($row);
                        if ($user) {
                            $importPreview[] = $user;
                        }
                    }
                }

                $this->view->assign('fields', $fields);
                $this->view->assign('fileName', $fileName);
                $this->view->assign('userFields', $userFields);
                $this->view->assign('table', $this->getTable());
                $this->view->assign('passwordsPlain', $this->getPasswordsPlain());
                $this->view->assign('updateOnly', $this->getUpdateOnly());

                $translatedUserFields = [];
                foreach ($userFields as $userField) {
                    $translatedUserFields[$userField] = preg_replace('/:$/ism', '', $this->translate($GLOBALS['TCA'][$this->getTable()]['columns'][$userField]['label'])) ?: '[' . $userField . ']';
                }
                $this->view->assign('translatedUserFields', $translatedUserFields);

                $this->view->assign('importPreview', $importPreview);
                $this->view->assign('morePreviewCount', count($importData) - 5);
                $this->view->assign('mapping', $mapping);
                $this->view->assign('canImport', count($this->controllerContext->getFlashMessageQueue()->getAllMessages()) > 0 ? false : true);
            }
        }

        $this->view->assign('pageData', $pageData);
    }

    public function importAction()
    {
        $this->controllerContext->getFlashMessageQueue()->getAllMessagesAndFlush();
        $pageData = $this->getPageData();

        if (!in_array($pageData['doktype'], [254, 199])) {
            $this->redirect('show');
        } else {
            $pageData['module'] = 'fe_users';
            $importData = $this->getImportData($fileName);
            $fields = $this->getFields($importData);

            if (!count($fields)) {
                $this->addError('No data found');
            } else {
                $userFields = $this->getUserFields(true);

                $this->getMapping($fields, $userFields);
                $users = $this->getImportedUsers($fields, $userFields, $importData);

                if (count($this->controllerContext->getFlashMessageQueue()->getAllMessages()) == 0) {
                    $imported = [
                        'new'     => 0,
                        'updated' => 0,
                    ];

                    $persistenceManager = $this->objectManager->get('TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager');
                    foreach ($users as $row) {
                        $user = $this->createUser($row);
                        if ($user) {
                            if ($user->getUid()) {
                                $this->userRepository->update($user);
                                $imported['updated']++;
                            } else {
                                $this->userRepository->add($user);
                                $imported['new']++;
                            }
                            $persistenceManager->persistAll();

                            $recData = [];
                            $recData[$this->getTable()][$user->getUid()]['diable'] = $user->getDisable() ? 1 : 0;

                            $this->initializeTceMain();
                            $this->tce->start($recData, []);
                            $this->tce->process_datamap();

                            $persistenceManager->clearState();
                        }
                    }

                    $this->view->assign('imported', $imported);
                } else {
                    $this->sessionStorage->set('imia_user-import', [
                        'fileName'       => $this->request->hasArgument('fileName') ? $this->request->getArgument('fileName') : null,
                        'field'          => $this->request->hasArgument('field') ? $this->request->getArgument('field') : null,
                        'specialMapping' => $this->request->hasArgument('specialMapping') ? $this->request->getArgument('specialMapping') : null,
                    ]);

                    $this->redirect('mapping', null, null, [
                        'importsession' => true,
                    ]);
                }
            }
        }

        $this->view->assign('pageData', $pageData);
    }

    /**
     * @return array
     */
    public function getSettings()
    {
        if (!$this->settings) {
            $typoScript = BackendTypoScript::get($this->getPageUid());
            $this->settings = $typoScript['plugin.']['tx_imiauser.']['settings.'];
        }

        return $this->settings;
    }

    /**
     * @param integer $pageUid
     * @return integer
     */
    public function getPageRootUid($pageUid = null)
    {
        if (!$pageUid) {
            $pageUid = $this->getPageUid();
        }

        $page = BackendUtility::getRecordWSOL('pages', $pageUid, 'uid,pid,is_siteroot');
        if (!$page['is_siteroot'] && $page['pid']) {
            $pageUid = $this->getPageRootUid($page['pid']);
        }

        return $pageUid;
    }

    /**
     * @return array|null
     */
    public function getPageData()
    {
        if (!$this->pageData) {
            $this->pageData = BackendUtility::getRecordWSOL('pages', $this->getPageUid(), '*');
            if (!in_array((int)$this->pageData['doktype'], [199, 254])) {
                $this->pageData['webView'] = true;
            }
        }

        return $this->pageData;
    }

    /**
     * @return integer
     */
    public function getPageUid()
    {
        return (int)GeneralUtility::_GP('id');
    }

    /**
     * @return boolean
     */
    protected function getPasswordsPlain()
    {
        return $this->request->hasArgument('passwordsPlain') ? (bool)$this->request->getArgument('passwordsPlain') : true;
    }

    /**
     * @return boolean
     */
    protected function getUpdateOnly()
    {
        return $this->request->hasArgument('updateOnly') ? (bool)$this->request->getArgument('updateOnly') : false;
    }

    /**
     * @param array $fields
     * @param array $userFields
     * @return array
     */
    protected function getMapping($fields, $userFields)
    {
        $userFieldsStatus = $this->getUserFieldsStatus($fields, $userFields);

        $specialMappingFields = [];
        foreach ($userFieldsStatus as $field => $userFieldStatus) {
            if (!$userFieldStatus['mapped']) {
                if (!$this->isMultipleValuesField($field)) {
                    $specialMapping = $this->getSpecialMapping($field);
                    $specialMappingValue = null;
                    if ($specialMapping == '#assignfixed#') {
                        $specialMappingValue = $this->getSpecialMappingValue($field);
                    }

                    $specialMappingFields[$field] = [
                        'field'      => $field,
                        'label'      => preg_replace('/:$/ism', '', $this->translate($GLOBALS['TCA'][$this->getTable()]['columns'][$field]['label'])) ?: '[' . $field . ']',
                        'value'      => $specialMapping ?: null,
                        'valueMap'   => $this->getFieldValueMap($field),
                        'fixedValue' => $specialMappingValue,
                    ];

                    if ($specialMapping) {
                        $userFieldsStatus[$field]['mapped'] = $userFieldStatus['mapped'] = true;
                    }
                }

                if ($userFieldStatus['required'] && !$userFieldStatus['mapped']) {
                    if ($this->getUpdateOnly()) {
                        $userFieldsStatus[$field]['mapped'] = $userFieldStatus['mapped'] = true;
                    } else {
                        $this->addError($this->translate('be_backend_mapping_import_field_error', [
                            preg_replace('/:^/ism', '', $this->translate($GLOBALS['TCA'][$this->getTable()]['columns'][$field]['label'])) ?: $field,
                        ]));
                    }
                }
            }
        }

        $mappingUserFields = [''];
        $specialMappingOptions = [
            'default' => [],
        ];

        foreach ($userFields as $userField) {
            $mappingUserFields[$userField] = preg_replace('/:$/ism', '', $this->translate($GLOBALS['TCA'][$this->getTable()]['columns'][$userField]['label'])) ?: '[' . $userField . ']';

            if (!array_key_exists($userField, $specialMappingFields) && !$this->isMultipleValuesField($userField)) {
                $specialMappingOptions['default'][$userField] = $this->translate('be_backend_specialmapping_infield') . $mappingUserFields[$userField];
            }
        }
        asort($mappingUserFields);

        $specialMappingOptions['password'] = array_merge(
            ['', 'password_generate' => $this->translate('be_backend_specialmapping_passwordgenerate')],
            $specialMappingOptions['default']
        );

        $specialMappingOptions['default'] = array_merge(
            ['', '#assignfixed#' => $this->translate('be_backend_specialmapping_assignfixed')],
            $specialMappingOptions['default']
        );

        // Hook: getMappingFields
        foreach ($this->getHookObjects() as $hookObject) {
            if (method_exists($hookObject, 'getMappingFields')) {
                $hookObject->getMappingFields($userFields, $mappingUserFields, $specialMappingFields,
                    $specialMappingOptions, $this);
            }
        }

        $specialMappingFieldsSort = [];
        foreach ($specialMappingFields as $key => $specialMappingField) {
            $specialMappingFields[$key]['options'] = array_key_exists($key, $specialMappingOptions)
                ? $specialMappingOptions[$key]
                : $specialMappingOptions['default'];

            asort($specialMappingFields[$key]['options']);
            $specialMappingFieldsSort[$key] = $specialMappingField['label'];
        }

        array_multisort($specialMappingFields, SORT_ASC, SORT_STRING, $specialMappingFieldsSort);

        return [
            'userfields'       => $mappingUserFields,
            'userfieldsStatus' => $userFieldsStatus,
            'specialfields'    => $specialMappingFields,
        ];
    }

    /**
     * @param array $fields
     * @param array $userFields
     * @return array
     */
    protected function getUserFieldsStatus($fields, $userFields)
    {
        $userFieldsStatus = [];
        foreach ($userFields as $userField) {
            $userFieldsStatus[$userField] = [
                'mapped'   => false,
                'required' => in_array($userField, ['username', 'password', 'email']) ? true : false,
            ];
        }

        foreach ($fields as $field) {
            if ($field['userfield'] && array_key_exists($field['userfield'], $userFieldsStatus)) {
                $userFieldsStatus[$field['userfield']]['mapped'] = true;
            }
        }

        return $userFieldsStatus;
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->userRepository->getTableName();
    }

    /**
     * @param string $field
     * @return string
     */
    protected function getSpecialMapping($field = null)
    {
        if (!$this->specialMapping) {
            if ($this->request->hasArgument('specialMapping') && is_array($this->request->getArgument('specialMapping'))) {
                $this->specialMapping = $this->request->getArgument('specialMapping');
            } elseif ($this->request->hasArgument('importsession') && $this->sessionStorage->has('imia_user-import')) {
                $sessionData = $this->sessionStorage->get('imia_user-import');
                $this->specialMapping = $sessionData['specialMapping'];
            }
        }

        if ($field) {
            return $this->specialMapping && is_array($this->specialMapping) && array_key_exists($field, $this->specialMapping) && $this->specialMapping[$field]
                ? $this->specialMapping[$field]
                : null;
        } else {
            return $this->specialMapping;
        }
    }

    /**
     * @param string $field
     * @return string
     */
    protected function getSpecialMappingValue($field = null)
    {
        if (!$this->specialMappingValue) {
            if ($this->request->hasArgument('specialMappingValue') && is_array($this->request->getArgument('specialMappingValue'))) {
                $this->specialMappingValue = $this->request->getArgument('specialMappingValue');
            }
        }

        if ($field) {
            return $this->specialMappingValue && is_array($this->specialMappingValue) && array_key_exists($field, $this->specialMappingValue) && $this->specialMappingValue[$field]
                ? $this->specialMappingValue[$field]
                : null;
        } else {
            return $this->specialMappingValue;
        }
    }

    /**
     * @param array $fields
     * @param array $importData
     * @param array $userFields
     * @return array
     */
    protected function getImportedUsers($fields, $userFields, $importData)
    {
        $users = [];
        array_shift($importData);

        foreach ($importData as $importRow) {
            $row = [];
            foreach ($userFields as $userField) {
                $row[$userField] = null;
            }

            foreach ($importRow as $key => $importValue) {
                if (array_key_exists($key, $fields)) {
                    $userField = $fields[$key]['userfield'];
                    if ($userField) {
                        $value = $row[$userField];

                        if ($value) {
                            if ($this->isMultipleValuesField($userField)) {
                                if ($fields[$key]['valueMap']) {
                                    foreach ($fields[$key]['valueMap']['values'] as $fieldValue) {
                                        if ($fieldValue['title'] == trim($importValue) && $fieldValue['option']) {
                                            $value .= ',' . $fieldValue['option'];
                                            break;
                                        }
                                    }
                                } else {
                                    $value .= ',' . $importValue;
                                }
                            } else {
                                $this->addError('error: field ' . $userField . ' with duplicate value');
                            }
                        } else {
                            if ($userField == 'usergroup') {
                                $importValues = explode('|', $importValue);
                                $value = [];

                                foreach ($fields[$key]['valueMap']['values'] as $fieldValue) {
                                    if ($fieldValue['title'] == '#all' && $fieldValue['option']) {
                                        $value[] = $fieldValue['option'];
                                        break;
                                    }
                                }

                                foreach ($importValues as $singleImportValue) {
                                    $singleValue = null;
                                    if ($fields[$key]['valueMap']) {
                                        foreach ($fields[$key]['valueMap']['values'] as $fieldValue) {
                                            if ($fieldValue['title'] == trim($singleImportValue) && $fieldValue['option']) {
                                                $singleValue = $fieldValue['option'];
                                                break;
                                            }
                                        }
                                    } else {
                                        $singleValue = $singleImportValue;
                                    }

                                    if ($singleValue == -1) {
                                        $singleValue = ['new' => ($fields[$key]['usergroupNewPrefix'] ?: '') . $singleImportValue];
                                    }

                                    $value[] = $singleValue;
                                }
                            } else {
                                if ($fields[$key]['valueMap']) {
                                    foreach ($fields[$key]['valueMap']['values'] as $fieldValue) {
                                        if ($fieldValue['title'] == trim($importValue)) {
                                            $value = $fieldValue['option'];
                                            break;
                                        }
                                    }
                                } else {
                                    $value = $importValue;
                                }
                            }
                        }

                        $row[$userField] = $value;
                    }
                }
            }

            $specialMapping = $this->getSpecialMapping();
            if ($specialMapping && is_array($specialMapping)) {
                foreach ($specialMapping as $field => $option) {
                    if ($option == 'password_generate') {
                        $row[$field] = substr(sha1(uniqid(md5(rand()), true)), 0, 12);
                    } elseif ($option == '#assignfixed#') {
                        $row[$field] = $this->getSpecialMappingValue($field);
                    } elseif ($row[$option]) {
                        $row[$field] = $row[$option];
                    }
                }
            }

            if ($this->settings['emailAsUsername']) {
                $row['username'] = $row['email'];
            }

            if (in_array($row['username'], $this->usernames)) {
                $this->addError('Duplicate username: ' . $row['username']);
            } else {
                $this->usernames[] = $row['username'];
            }

            $users[] = $row;
        }

        return $users;
    }

    /**
     * @param array $row
     * @return \IMIA\ImiaUser\Domain\Model\User
     */
    protected function createUser($row)
    {
        if ($this->settings['emailAsUsername']) {
            $row['username'] = $row['email'];
        }

        $user = $this->userRepository->findOneByUsername($row['username']);
        if (!$user) {
            $user = $this->userRepository->findOneByEmail($row['email']);
            if (!$user && !$this->getUpdateOnly()) {
                $user = GeneralUtility::makeInstance(User::class);
            } elseif ($this->getUpdateOnly()) {
                return null;
            }
        }

        // Hook: createUser
        foreach ($this->getHookObjects() as $hookObject) {
            if (method_exists($hookObject, 'createUser')) {
                $hookObject->createUser($user, $row, $this->userRepository, $this);
            }
        }

        if (array_key_exists('usergroup', $row) && $row['usergroup'] !== null) {
            $user->setUsergroup(new ObjectStorage);
        }

        foreach ($row as $field => $value) {
            if ($value !== null) {
                $methodName = 'set' . GeneralUtility::underscoredToUpperCamelCase($field);

                if (method_exists($user, $methodName)) {
                    switch ($field) {
                        case 'country':
                            $country = $this->countryRepository->findOneByUid($value);
                            if ($country) {
                                $user->$methodName($country);
                            }
                            break;
                        case 'password':
                            if (!$user->getUid() || $this->getSpecialMapping($field) != 'password_generate') {
                                $user->$methodName($value, $this->getPasswordsPlain());
                            }
                            break;
                        case 'username':
                            $user->$methodName($value);
                            break;
                        case 'usergroup':
                            foreach ($value as $singleValue) {
                                if (is_array($singleValue)) { // create new usergroups
                                    $key = $singleValue['new'];
                                    if (!array_key_exists($key, $this->usergroups)) {
                                        $usergroup = GeneralUtility::makeInstance(UserGroup::class);
                                        $usergroup->setPid($user->getPid());
                                        $usergroup->setTitle($singleValue['new']);

                                        $this->usergroups[$key] = $usergroup;
                                    }
                                } else {
                                    $key = $singleValue;
                                    if (!array_key_exists($key, $this->usergroups)) {
                                        $this->usergroups[$key] = $this->userGroupRepository->findOneByUid($singleValue);
                                    }
                                }

                                if ($this->usergroups[$key]) {
                                    $user->addUsergroup($this->usergroups[$key]);
                                }
                            }
                            break;
                        default:
                            $user->$methodName($value);
                            break;
                    }
                }
            }
        }

        // Hook: updateUser
        foreach ($this->getHookObjects() as $hookObject) {
            if (method_exists($hookObject, 'updateUser')) {
                $hookObject->updateUser($row, $user, $this);
            }
        }

        return $user;
    }

    /**
     * @param string $userField
     * @return boolean
     */
    protected function isMultipleValuesField($userField)
    {
        switch ($userField) {
            default:
                $multiple = false;
                break;
        }

        // Hook: isMultipleValuesField
        foreach ($this->getHookObjects() as $hookObject) {
            if (method_exists($hookObject, 'isMultipleValuesField')) {
                $hookObject->isMultipleValuesField($userField, $multiple, $this);
            }
        }

        return $multiple;
    }

    /**
     * @param array $importData
     * @return array
     */
    protected function getFields($importData)
    {
        $fields = [];
        $fieldRow = array_shift($importData);
        $fieldData = array_values($importData);

        $mappingKey = sha1(implode(',', $fieldRow) . '_' . implode(',', array_keys($fieldRow)));
        $cache = new Cache('imia_user-import-' . $mappingKey);

        if ($this->request->hasArgument('file')) {
            $cache->write(null);
        }

        $mapping = null;
        if ($this->request->hasArgument('field') && is_array($this->request->getArgument('field'))) {
            $mapping = $this->request->getArgument('field');

            $cache->write([
                $mapping,
                $this->getSpecialMapping(),
                $this->getSpecialMappingValue(),
            ]);
        } elseif ($this->request->hasArgument('importsession') && $this->sessionStorage->has('imia_user-import')) {
            $sessionData = $this->sessionStorage->get('imia_user-import');
            $mapping = $sessionData['field'];

            $cache->write([
                $mapping,
                $this->getSpecialMapping(),
                $this->getSpecialMappingValue(),
            ]);
        } else {
            if ($cache->exists()) {
                list($mapping, $this->specialMapping, $this->specialMappingValue) = $cache->load();
            }
        }

        foreach ($fieldRow as $key => $fieldName) {
            if (trim($fieldName)) {
                $fields[$key] = [
                    'title'     => $fieldName,
                    'example'   => $importData[0][$key],
                    'userfield' => is_array($mapping) ? $mapping[$key]['userfield'] : $this->guessMapping($fieldName),
                    'valueMap'  => false,
                ];

                if ($fields[$key]['userfield'] == 'usergroup') {
                    $fields[$key]['usergroupNewPrefix'] = is_array($mapping) ? $mapping[$key]['usergroupNewPrefix'] : '';
                }

                $field = &$fields[$key];
                if (!in_array($field['userfield'], $this->getUserFields(true))) {
                    $field['userfield'] = '';
                }

                switch ($field['userfield']) {
                    case 'country':
                        $countryOptions = [''];
                        /** @var Country $country */
                        foreach ($this->getCountries() as $country) {
                            $countryOptions[$country->getUid()] = ($GLOBALS['LANG']->lang == 'de' && $country->getShortDe())
                                ? $country->getShortDe()
                                : $country->getShortEn();
                        }
                        asort($countryOptions);

                        $field['valueMap'] = [
                            'options' => $countryOptions,
                        ];
                        break;
                    default:
                        $field['valueMap'] = $this->getFieldValueMap($field['userfield']);
                        break;
                }

                // Hook: getFieldSettings
                foreach ($this->getHookObjects() as $hookObject) {
                    if (method_exists($hookObject, 'getFieldSettings')) {
                        $hookObject->getFieldSettings($field, $this);
                    }
                }
            }
        }

        foreach ($fieldData as $data) {
            foreach ($data as $key => $value) {
                if ($fields[$key]['valueMap']) {
                    if (!$fields[$key]['valueMap']['values']) {
                        $fields[$key]['valueMap']['values'] = [];
                    }
                    $fields[$key]['valueMap']['values'][] = trim($value);
                }
            }
        }

        foreach ($fields as $key => $val) {
            if ($fields[$key]['valueMap']) {
                $values = array_unique($fields[$key]['valueMap']['values']);
                natcasesort($values);

                if ($fields[$key]['userfield'] == 'usergroup') {
                    $newValues = [];
                    foreach ($values as $value) {
                        $newValues = array_merge($newValues, array_map('trim', explode('|', $value)));
                    }
                    $values = array_unique($newValues);
                    natcasesort($values);

                    array_unshift($values, '#all');
                }

                $fields[$key]['valueMap']['values'] = [];
                foreach ($values as $value) {
                    $valueMapping = [
                        'title'  => $value,
                        'option' => is_array($mapping) && $mapping[$key]['option']
                            ? $mapping[$key]['option'][$value] ? $mapping[$key]['option'][$value] : $mapping[$key]['option'][(int)$value]
                            : $this->guessMapping($fields[$key]['title'], $value),
                    ];

                    if ($fields[$key]['userfield'] == 'usergroup') {
                        if (!$valueMapping['option'] && $valueMapping['title'] && $valueMapping['title'] != '#all') {
                            $valueMapping['option'] = -1;
                        }
                    }

                    $fields[$key]['valueMap']['values'][] = $valueMapping;
                }
            }
        }

        return $fields;
    }

    /**
     * @param string $field
     * @return array|boolean
     */
    protected function getFieldValueMap($field)
    {
        $valueMap = false;
        $tcaColumnConfig = $GLOBALS['TCA'][$this->getTable()]['columns'][$field]['config'];
        if (in_array($tcaColumnConfig['type'], ['select', 'radio'])) {
            $valueMap = ['options' => []];

            if ((int)$tcaColumnConfig['maxitems'] > 1) {
                $valueMap['options'][''] = '';
            }

            if (isset($tcaColumnConfig['items']) && is_array($tcaColumnConfig['items'])) {
                foreach ($tcaColumnConfig['items'] as $item) {
                    $valueMap['options'][$item[1]] = $this->translate($item[0]);
                }
            }

            if ($tcaColumnConfig['foreign_table']) {
                $foreignTCA = $GLOBALS['TCA'][$tcaColumnConfig['foreign_table']]['ctrl'];

                $TSConfig = BackendUtility::getTCEFORM_TSconfig('pages', BackendUtility::getRecordWSOL('pages', $this->getPageUid()));
                $TSConfig['_THIS_ROW'] = [];
                $TSConfig['_CURRENT_PID'] = $TSConfig['_THIS_UID'];
                $TSConfig['_THIS_UID'] = null;

                $foreignTableWhere = BackendUtility::replaceMarkersInWhereClause(
                    ($tcaColumnConfig['foreign_table_where']), $this->getTable(), $field, $TSConfig
                );

                $foreignResult = $this->getDb()->exec_SELECTquery(
                    '*',
                    $tcaColumnConfig['foreign_table'],
                    '1=1 ' . ($foreignTCA['delete'] ? 'AND ' . $foreignTCA['delete'] . ' = 0 ' : '') . $foreignTableWhere
                );
                while ($foreignRow = $this->getDb()->sql_fetch_assoc($foreignResult)) {
                    $label = $foreignRow[$foreignTCA['label']];
                    if ($foreignTCA['label_alt_force']) {
                        $labelAlt = array_map('trim', explode(',', $foreignTCA['label_alt']));
                        $labels = [$label];
                        foreach ($labelAlt as $labelAltField) {
                            $labels[] = $foreignRow[$labelAltField];
                        }
                        $label = implode(', ', $labels);
                    }

                    $valueMap['options'][$foreignRow['uid']] = $label;
                }
            }
        }

        return $valueMap;
    }

    /**
     * @param string $fileName
     * @return array
     */
    protected function getImportData(&$fileName)
    {
        $importData = null;
        $tempPath = PATH_site . 'typo3temp/imia_user';
        if (!is_dir($tempPath)) {
            @mkdir($tempPath);
        }

        $filePath = null;
        $fileData = $this->getFileArgument('file');
        if ($fileData) {
            $fileName = $fileData['name'];
            $filePath = $tempPath . '/' . $fileName;
            if (!@copy($fileData['tmp_name'], $filePath)) {
                $filePath = null;
                $this->addError('Writing uploaded file failed');
            }
        } elseif ($this->request->hasArgument('fileName')) {
            $fileName = $this->request->getArgument('fileName');
            $filePath = $tempPath . '/' . $fileName;

            if (!file_exists($filePath)) {
                $filePath = null;
                $this->addError('File not found');
            }
        } elseif ($this->request->hasArgument('importsession') && $this->sessionStorage->has('imia_user-import')) {
            $sessionData = $this->sessionStorage->get('imia_user-import');
            $fileName = $sessionData['fileName'];
            $filePath = $tempPath . '/' . $fileName;

            if (!file_exists($filePath)) {
                $filePath = null;
                $this->addError('File not found');
            }
        }

        if ($filePath) {
            try {
                $importData = PHPExcel_IOFactory::load($filePath)
                    ->getActiveSheet()
                    ->toArray(null, true, true, true);
            } catch (\Exception $e) {
                $this->addError($e->getMessage());
            }
        }

        return $importData;
    }

    /**
     * @param string $field
     * @param string $value
     * @return string
     */
    protected function guessMapping($field, $value = null)
    {
        switch (strtolower(trim($field))) {
            case 'country':
            case 'land':
                $field = 'country';
                break;
            case 'city':
            case 'town':
            case 'stadt':
            case 'ort':
            case 'wohnort':
                $field = 'city';
                break;
            case 'salutation':
            case 'gender':
            case 'anrede':
            case 'geschlecht':
                $field = 'gender';
                break;
            case 'first name':
            case 'firstname':
            case 'vorname':
                $field = 'first_name';
                break;
            case 'last name':
            case 'lastname':
            case 'family name':
            case 'nachname':
            case 'familienname':
                $field = 'last_name';
                break;
            case 'name':
                $field = 'name';
                break;
            case 'email':
            case 'e-mail':
            case 'mail':
                $field = 'email';
                break;
            case 'telephone':
            case 'phone':
            case 'telefon':
                $field = 'telephone';
                break;
            case 'username':
            case 'login':
            case 'benutzername':
                $field = 'username';
                break;
            case 'address':
            case 'adresse':
            case 'anschrift':
            case 'straße':
            case 'strasse':
                $field = 'address';
                break;
            case 'plz':
            case 'postcode':
            case 'zip':
                $field = 'zip';
                break;
            case 'fax':
            case 'telefax':
                $field = 'fax';
                break;
            case 'mobile':
            case 'mobil':
            case 'mobile phone':
            case 'handy':
                $field = 'mobile';
                break;
            case 'company':
            case 'firma':
            case 'unternehmen':
            case 'unternehmung':
            case 'partner':
                $field = 'company';
                break;
            case 'position':
                $field = 'position';
                break;
            case 'password':
            case 'passwort':
            case 'kennwort':
                $field = 'password';
                break;
            case 'lastlogin':
            case 'last login':
            case 'last_login':
            case 'letzter login':
            case 'lastvisit':
            case 'last visit':
            case 'lastvisitdate':
                $field = 'lastlogin';
                break;
            case 'crdate':
            case 'created':
            case 'registered':
            case 'registerdate':
                $field = 'crdate';
                break;
            case 'usergroup':
            case 'usergroups':
            case 'group':
            case 'groups':
                $field = 'usergroup';
                break;
            default:
                $field = '';
                break;
        }

        if ($field && $value !== null) {
            $option = '';
            switch ($field) {
                case 'gender':
                    switch (strtolower(trim($value))) {
                        case 'male':
                        case 'männlich':
                        case 'mann':
                        case 'man':
                        case 'mr.':
                        case 'mr':
                        case 'herr':
                            $option = 'm';
                            break;
                        case 'female':
                        case 'woman':
                        case 'mrs.':
                        case 'mrs':
                        case 'frau':
                            $option = 'f';
                            break;
                    }
                    break;
                case 'country':
                    /** @var Country $country */
                    foreach ($this->getCountries() as $country) {
                        if (in_array(strtolower(trim($value)), [
                            strtolower($country->getShortEn()),
                            strtolower($country->getShortDe()),
                            strtolower($country->getShortLocal()),
                            strtolower($country->getOfficialNameLocal()),
                            strtolower($country->getOfficialNameEn()),
                        ])) {
                            $option = $country->getUid();
                            break;
                        }
                    }
                    break;
                default:
                    $valueMap = $this->getFieldValueMap($field);
                    if ($valueMap && isset($valueMap['options']) && is_array($valueMap['options'])) {
                        foreach ($valueMap['options'] as $key => $mapValue) {
                            if (strtolower($value) == strtolower($mapValue)) {
                                $option = $key;
                                break;
                            }
                        }

                        if (!$option) {
                            foreach ($valueMap['options'] as $key => $mapValue) {
                                if (stripos($mapValue, $value) !== false) {
                                    $option = $key;
                                    break;
                                }
                            }
                        }
                    }
                    break;
            }

            return $option;
        } else {
            return $field;
        }
    }

    /**
     * @return array
     */
    protected function getCountries()
    {
        if (!$this->countries) {
            $this->countries = $this->countryRepository->findAll();
        }

        return $this->countries;
    }

    /**
     * @return array
     */
    protected function getUserFields($password = false)
    {
        if (!$this->userFields) {
            $this->userFields = ['username', 'password', 'crdate', 'usergroup', 'lastlogin', 'name'];

            $settings = $this->getSettings();
            foreach (explode(',', $settings['fields.']['enabled']) as $field) {
                $this->userFields[] = GeneralUtility::camelCaseToLowerCaseUnderscored(trim($field));
            }

            // Hook: getUserFields
            foreach ($this->getHookObjects() as $hookObject) {
                if (method_exists($hookObject, 'getUserFields')) {
                    $hookObject->getUserFields($this->userFields, $password, $this);
                }
            }
        }

        if (!$password && in_array('password', $this->userFields)) {
            $this->userFields = array_diff($this->userFields, ['password']);
        }

        if ($this->settings['emailAsUsername']) {
            $ufc = count($this->userFields);
            $this->userFields = array_diff($this->userFields, ['username']);
            if ($ufc < count($this->userFields)) {
                $this->userFields[] = 'email';
            }
        }

        return array_unique($this->userFields);
    }
}