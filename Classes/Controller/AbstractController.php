<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaUser\Controller;

use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\DataHandling\DataHandler;

/**
 * @package     imia_user
 * @subpackage  Controller
 * @author      David Frerich <d.frerich@imia.de>
 */
class AbstractController extends \IMIA\ImiaBaseExt\Controller\ActionController
{
    /**
     * @var \IMIA\ImiaUser\Domain\Repository\UserRepository
     * @inject
     */
    protected $userRepository;

    /**
     * @var \IMIA\ImiaUser\Domain\Repository\UserGroupRepository
     * @inject
     */
    protected $userGroupRepository;

    /**
     * @var \IMIA\ImiaUser\Domain\Repository\CountryRepository
     * @inject
     */
    protected $countryRepository;

    /**
     * @var \IMIA\ImiaUser\Domain\Repository\LanguageRepository
     * @inject
     */
    protected $languageRepository;

    /**
     * @var DataHandler
     */
    protected $tce;

    /**
     * @return string
     */
    public function getTable()
    {
        return 'fe_users';
    }

    /**
     * @param string $key
     * @param string $field
     * @param array $arguments
     */
    public function addError($key, $field = null, $arguments = null)
    {
        $this->addFlashMessage($this->translate($key, $arguments) ?: $key, $field, AbstractMessage::ERROR, false);
    }

    /**
     * @param string $key
     * @param string $field
     * @param array $arguments
     */
    public function addSuccess($key, $field = null, $arguments = null)
    {
        $this->addFlashMessage($this->translate($key, $arguments) ?: $key, $field, AbstractMessage::OK, false);
    }

    /**
     * Initializes \TYPO3\CMS\Core\DataHandling\DataHandler
     */
    protected function initializeTceMain()
    {
        if (!isset($this->tce)) {
            $this->tce = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\DataHandling\\DataHandler');
            $this->tce->enableLogging = false;
            $this->tce->stripslashes_values = 0;
        }
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}