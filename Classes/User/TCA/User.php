<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaUser\User\TCA;

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * @package     imia_user
 * @subpackage  User
 * @author      David Frerich <d.frerich@imia.de>
 */
class User
{
    /**
     * @param array $params
     * @param object $pObj
     */
    public function country(&$params, &$pObj)
    {
        $labelField = 'cn_short_local';
        if ($GLOBALS['LANG'] && $GLOBALS['LANG']->lang) {
            $langKey = $GLOBALS['LANG']->lang;
            if ($langKey == 'en') {
                $labelField = 'cn_short_en';
            } elseif (ExtensionManagementUtility::isLoaded('static_info_tables_' . $langKey)) {
                $labelField = 'cn_short_' . $langKey;
            }
        }

        $result = $GLOBALS['TYPO3_DB']->exec_SELECTquery('uid, ' . $labelField, 'static_countries', '1=1', '', $labelField . ' ASC');
        while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result)) {
            $params['items'][] = [$row[$labelField], $row['uid']];
        }
    }

    /**
     * @param array $params
     * @param object $pObj
     * @return string
     */
    public function registerChecks(&$params, &$pObj)
    {
        $row = $this->getRow($params);
        $content = '';
        if ($row['register_checks']) {
            $registerChecks = json_decode($row['register_checks'], true);

            if (is_array($registerChecks) && count($registerChecks)) {
                foreach ($registerChecks as $registerCheck) {
                    $content .= '<div class="checkbox disabled"><label><input type="checkbox" value="1" checked="checked" disabled="disabled">&nbsp;' . $registerCheck['text'] . '</label></div>';
                }
            }
        }

        return $content;
    }

    /**
     * @param array $params
     * @return array
     */
    protected function getRow(array $params)
    {
        $row = [];
        foreach ($params['row'] as $key => $val) {
            if (is_array($val)) {
                $row[$key] = implode(',', $val);
            } else {
                $row[$key] = $val;
            }
        }

        return $row;
    }
}