<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaUser\Utility;

/**
 * @package     imia_user
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class Cache
{
    /**
     * @var array
     */
    static protected $caches = [];

    /**
     * @var string
     */
    protected $cacheKey;

    /**
     * @var boolean
     */
    protected $permanent;

    /**
     * @param string $cache
     * @param boolean $permanent
     * @return string
     */
    static public function getCachePath($cache, $permanent = false)
    {
        $path = PATH_site . 'typo3temp/Cache/Code/' . ($permanent ? 'cache_imiauser' : 'cache_core');
        if (!is_dir($path)) {
            @mkdir($path);
        }

        return $path . '/' . $cache . '.php';
    }

    /**
     * @param string $cache
     * @param boolean $permanent
     * @return boolean
     */
    static public function has($cache, $permanent = false)
    {
        return self::get($cache, $permanent) ? true : false;
    }

    /**
     * @param string $cache
     * @param boolean $permanent
     * @return mixed
     */
    static public function get($cache, $permanent = false)
    {
        ob_start();
        if (!array_key_exists($cache, self::$caches)) {
            @include(self::getCachePath($cache, $permanent));
            @self::$caches[$cache] = $a;
        }
        ob_end_clean();

        return self::$caches[$cache];
    }

    /**
     * @param string $cache
     * @param mixed $contents
     * @param boolean $permanent
     */
    static public function put($cache, $contents, $permanent = false)
    {
        @file_put_contents(self::getCachePath($cache, $permanent), '<?php' . "\n" . '$a = unserialize(\'' .
            str_replace("'", "\'", serialize($contents)) . '\');');
    }

    /**
     * @param string $cacheKey
     * @param boolean $permanent
     */
    public function __construct($cacheKey, $permanent = false)
    {
        $this->cacheKey = $cacheKey;
        $this->permanent = $permanent;

        if (strpos($cacheKey, '/')) {
            $this->mkdir(dirname(self::getCachePath($cacheKey, $permanent)));
        }
    }

    /**
     * @return boolean
     */
    public function exists()
    {
        return self::has($this->cacheKey, $this->permanent);
    }

    /**
     * @return mixed
     */
    public function load()
    {
        return self::get($this->cacheKey, $this->permanent);
    }

    /**
     * @param mixed $contents
     */
    public function write($contents)
    {
        self::put($this->cacheKey, $contents, $this->permanent);
    }

    /**
     * @param string $dir
     */
    protected function mkdir($dir)
    {
        if (!is_dir($dir)) {
            $this->mkdir(dirname($dir));
            @mkdir($dir);
        }
    }
}