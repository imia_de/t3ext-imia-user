<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaUser\Xclass\Powermail\ViewHelpers\Misc;

use In2code\Powermail\Utility\FrontendUtility;

/**
 * @package     imia_user
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class PrefillFieldViewHelper extends \In2code\Powermail\ViewHelpers\Misc\PrefillFieldViewHelper
{
    /**
     * @param string $value
     * @return string
     */
    protected function getFromFrontendUser($value)
    {
        if (empty($value) && $this->getField()->getFeuserValue()) {
            if ($this->getField()->getFeuserValue() == 'name') {
                $value = FrontendUtility::getPropertyFromLoggedInFrontendUser('first_name') . ' ' .
                    FrontendUtility::getPropertyFromLoggedInFrontendUser('last_name');
            } else {
                $value = parent::getFromFrontendUser($value);
            }
        }

        return $value;
    }
}
