<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaUser\Domain\Model;

use IMIA\ImiaBaseExt\Annotation\SQL;
use IMIA\ImiaBaseExt\Annotation\TCA;
use IMIA\ImiaBaseExt\Utility\Helper;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMap;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;

/**
 * @package     imia_user
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @TCA\Table(
 *  types = {"username, password, lastlogin, --palette--;;gtc, register_checks, --div--;LLL::user_tab_access, disable, usergroup, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:fe_users.tabs.personelData, --palette--;LLL::user_name;name, --palette--;;company, --palette--;;address, --palette--;LLL::user_contact;contact, --div--;LLL::user_tab_description, description, image, pdf"},
 *  palettes = {
 *      "name": "gender, --linebreak--, first_name, last_name",
 *      "company": "company, --linebreak--, position",
 *      "address": "address, --linebreak--, zip, city, --linebreak--, country",
 *      "contact": "telephone, --linebreak--, mobile, --linebreak--, fax, --linebreak--, email",
 *      "gtc": "gtc, --linebreak--, gtc_time",
 *  }
 * )
 */
class User extends \IMIA\ImiaBaseExt\Domain\Model\FrontendUser
{
    /**
     * @var \TYPO3\CMS\Core\Resource\FileRepository
     * @inject
     * @FIXME How to do this a better way?
     */
    protected $fileRepository;

    /**
     * @var DataMap
     */
    protected $dataMap;

    /**
     * @var integer
     */
    protected $_localizedLanguageUid;

    /**
     * @var string
     */
    protected $table = 'fe_users';

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\IMIA\ImiaUser\Domain\Model\UserGroup>
     */
    protected $usergroup;

    /**
     * @SQL\Column(type="varchar", length=12, nullable=false)
     * @TCA\Select(
     *  label = "LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender",
     *  items = {
     *      {"LLL::user_gender_none", ""},
     *      {"LLL::user_gender_f", "f"},
     *      {"LLL::user_gender_m", "m"}
     *  },
     *  default = ""
     * )
     *
     * @var string
     */
    protected $gender = '';

    /**
     * @TCA\SearchField
     * @TCA\Input(size=12)
     *
     * @var string
     */
    protected $firstName = '';

    /**
     * @TCA\SearchField
     * @TCA\Input(size=12)
     *
     * @var string
     */
    protected $lastName = '';

    /**
     * @TCA\SearchField
     *
     * @var string
     */
    protected $address = '';

    /**
     * @TCA\SearchField
     *
     * @var string
     */
    protected $zip = '';

    /**
     * @TCA\SearchField
     *
     * @var string
     */
    protected $city = '';

    /**
     * @TCA\DateTime
     *
     * @var \DateTime
     */
    protected $crdate;

    /**
     * @TCA\DateTime
     *
     * @var \DateTime
     */
    protected $tstamp;

    /**
     * @SQL\Column(type="int", length=11, nullable=false)
     * @TCA\Select(
     *  items = {
     *      {"", 0}
     *  },
     *  foreign_table = "static_countries",
     *  itemsProcFunc = "IMIA\ImiaUser\User\TCA\User->country",
     *  size = 1,
     *  minitems = 0,
     *  maxitems = 1,
     *  default = "54"
     * )
     *
     * @var \IMIA\ImiaUser\Domain\Model\Country
     */
    protected $country;

    /**
     * @TCA\SearchField
     *
     * @var string
     */
    protected $company = '';

    /**
     * @SQL\Column(type="varchar", length=255, nullable=false)
     * @TCA\Input
     * @TCA\SearchField
     *
     * @var string
     */
    protected $position = '';

    /**
     * @SQL\Column(type="varchar", length=255)
     * @TCA\SearchField
     *
     * @var string
     */
    protected $email = '';

    /**
     * @TCA\SearchField
     *
     * @var string
     */
    protected $telephone = '';

    /**
     * @SQL\Column(type="varchar", length=255, nullable=false)
     * @TCA\Input
     *
     * @var string
     */
    protected $mobile = '';

    /**
     * @SQL\Column(type="tinyint", length=2, nullable=false)
     * @TCA\Check
     *
     * @var boolean
     */
    protected $confirmed = false;

    /**
     * @SQL\Column(type="varchar", length=32, nullable=true)
     * @TCA\Passthrough
     *
     * @var string
     */
    protected $confirmToken = null;

    /**
     * @SQL\Column(type="varchar", length=32, nullable=true)
     * @TCA\Passthrough
     *
     * @var string
     */
    protected $activateToken = null;

    /**
     * @SQL\Column(type="varchar", length=32, nullable=true)
     * @TCA\Select(
     *  items = {
     *      {"", ""}
     *  },
     *  foreign_table = "sys_languages"
     * )
     *
     * @var \IMIA\ImiaUser\Domain\Model\Language
     */
    protected $language;

    /**
     * @TCA\FilesImage(
     *  maxitems = 1
     * )
     *
     * @var integer
     */
    protected $image;

    /**
     * @SQL\Column(type="int", length=11)
     * @TCA\FilesPdf(
     *  maxitems = 1
     * )
     *
     * @var integer
     */
    protected $pdf;

    /**
     * @SQL\Column(type="text")
     * @TCA\SearchField
     * @TCA\Text
     *
     * @var string
     */
    protected $description = '';

    /**
     * @var boolean
     */
    protected $disable;

    /**
     * @SQL\Column(type="tinyint", length=3, default="0")
     * @TCA\Check(
     *   items = {
     *     {"LLL::user_gtc_check"},
     *   },
     *   readOnly = true,
     * )
     *
     * @var boolean
     */
    protected $gtc;

    /**
     * @SQL\Column(type="int", length=11, nullable=true)
     * @TCA\InputDateTime(
     *  displayCond = "FIELD:gtc_time:REQ:true",
     *  readOnly = true
     * )
     *
     * @var \DateTime
     */
    protected $gtcTime;

    /**
     * @TCA\Passthrough
     *
     * @var integer
     */
    protected $pid;

    /**
     * @SQL\Column(type="text")
     * @TCA\User(
     *     displayCond = "FIELD:register_checks:REQ:true",
     *     userFunc = "IMIA\ImiaUser\User\TCA\User->registerChecks",
     * )
     *
     * @var string
     */
    protected $registerChecks;

    public function __construct()
    {
        parent::__construct();

        $this->usergroup = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     * @return $this
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param string $position
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     * @return $this
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getDisable()
    {
        return $this->disable;
    }

    /**
     * @param boolean $disable
     * @return $this
     */
    public function setDisable($disable)
    {
        $this->disable = $disable;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * @param boolean $confirmed
     * @return $this
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfirmToken()
    {
        return $this->confirmToken;
    }

    /**
     * @return $this
     */
    public function generateConfirmToken()
    {
        $this->confirmToken = substr(sha1(md5(uniqid(rand(), true))), 0, 32);

        return $this;
    }

    /**
     * @return $this
     */
    public function clearConfirmToken()
    {
        $this->confirmToken = null;

        return $this;
    }

    /**
     * @return string
     */
    public function getActivateToken()
    {
        return $this->activateToken;
    }

    /**
     * @return $this
     */
    public function generateActivateToken()
    {
        $this->activateToken = substr(sha1(md5(uniqid(rand(), true))), 0, 32);

        return $this;
    }

    /**
     * @return $this
     */
    public function clearActivateToken()
    {
        $this->activateToken = null;

        return $this;
    }

    /**
     * @return \IMIA\ImiaUser\Domain\Model\Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param \IMIA\ImiaUser\Domain\Model\Language $language
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\IMIA\ImiaUser\Domain\Model\UserGroup>
     */
    public function getUsergroup()
    {
        return $this->usergroup;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\IMIA\ImiaUser\Domain\Model\UserGroup> $usergroup
     * @return $this
     */
    public function setUsergroup(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $usergroup)
    {
        $this->usergroup = $usergroup;

        return $this;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FrontendUserGroup $usergroup
     * @return $this
     */
    public function addUsergroup(\TYPO3\CMS\Extbase\Domain\Model\FrontendUserGroup $usergroup)
    {
        $this->usergroup->attach($usergroup);

        return $this;
    }

    /**
     * @return FileReference
     */
    public function getImage()
    {
        if (!$this->image || !is_object($this->image)) {
            $files = $this->fileRepository->findByRelation('fe_users', 'image', $this->getUid());
            $this->image = array_shift($files);
        }

        return $this->image;
    }

    /**
     * @param File $file
     * @return $this
     */
    public function setImage($file)
    {
        if ($this->getImage()) {
            $this->updateImage($this->getImage(), $file);
        } else {
            $this->image = $this->addImage($file);
        }

        return $this;
    }

    /**
     *
     * @param File $file
     * @param array $properties
     * @return FileReference
     */
    public function addImage(File $file, $properties = [])
    {
        $this->image = Helper::createFileReference($file, $properties, [
            'pid'         => $this->getPid(),
            'uid_foreign' => $this->getUid(),
            'tablenames'  => $this->table,
            'fieldname'   => 'image',
        ]);

        return $this->image;
    }

    /**
     * @param FileReference $fileReference
     * @param File $file
     * @return $this
     */
    public function updateImage(FileReference &$fileReference, File $file)
    {
        Helper::updateFileReference($fileReference, $file, true);

        return $this;
    }

    /**
     * @return $this
     */
    public function removeImage()
    {
        if ($this->getImage()) {
            Helper::deleteFileReference($this->getImage());

            $this->image = null;
        }

        return $this;
    }

    /**
     * @return FileReference
     */
    public function getPdf()
    {
        if (!$this->pdf || !is_object($this->pdf)) {
            $files = $this->fileRepository->findByRelation('fe_users', 'pdf', $this->getUid());
            $this->pdf = array_shift($files);
        }

        return $this->pdf;
    }

    /**
     * @param File $file
     * @return $this
     */
    public function setPdf(File $file)
    {
        if ($this->getPdf()) {
            $this->updatePdf($this->getPdf(), $file);
        } else {
            $this->pdf = $this->addPdf($file);
        }

        return $this;
    }

    /**
     *
     * @param File $file
     * @param array $properties
     * @return FileReference
     */
    public function addPdf(File $file, $properties = [])
    {
        $this->pdf = Helper::createFileReference($file, $properties, [
            'pid'         => $this->getPid(),
            'uid_foreign' => $this->getUid(),
            'tablenames'  => $this->table,
            'fieldname'   => 'pdf',
        ]);

        return $this->pdf;
    }

    /**
     * @param FileReference $fileReference
     * @param File $file
     * @return $this
     */
    public function updatePdf(FileReference &$fileReference, File $file)
    {
        Helper::updateFileReference($fileReference, $file, true);

        return $this;
    }

    /**
     * @return $this
     */
    public function removePdf()
    {
        if ($this->getPdf()) {
            Helper::deleteFileReference($this->getPdf());

            $this->pdf = null;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if ($this->getFirstName() && $this->getLastName()) {
            return $this->getFirstName() . ' ' . $this->getLastName() . ' (' . $this->getEmail() . ')';
        } else {
            return $this->getEmail();
        }
    }

    /**
     * @param boolean $gtc
     * @return $this
     */
    public function setGtc($gtc)
    {
        $this->gtc = $gtc;
        if ($gtc) {
            $this->gtcTime = new \DateTime('now');
        } else {
            $this->gtcTime = null;
        }

        return $this;
    }

    /**
     * @return boolean
     */
    public function getGtc()
    {
        return $this->gtc;
    }

    /**
     * @param \DateTime $gtcTime
     * @return $this
     */
    public function setGtcTime($gtcTime)
    {
        $this->gtcTime = $gtcTime;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getGtcTime()
    {
        return $this->gtcTime;
    }

    /**
     * @return int
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @param int $pid
     * @return User
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * @return array
     */
    public function getRegisterChecks()
    {
        if ($this->registerChecks) {
            return json_decode($this->registerChecks, true);
        } else {
            return [];
        }
    }

    /**
     * @param array $registerChecks
     * @return $this
     */
    public function setRegisterChecks($registerChecks)
    {
        if (is_array($registerChecks) && count($registerChecks) > 0) {
            $this->registerChecks = json_encode($registerChecks);
        } else {
            $this->registerChecks = '';
        }

        return $this;
    }

    /**
     * @param \DateTime $lastlogin
     * @return $this
     */
    public function setLastlogin(\DateTime $lastlogin)
    {
        $this->lastlogin = $lastlogin;

        return $this;
    }

    /**
     * @param \DateTime $crdate
     * @return $this
     */
    public function setCrdate($crdate)
    {
        if (!($crdate instanceof \DateTime)) {
            $crdate = new \DateTime((string)$crdate);
        }

        $this->crdate = $crdate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCrdate()
    {
        return $this->crdate;
    }

    /**
     * @return \DateTime
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $nameArr = explode(' ', $name, 2);
        if (count($nameArr) == 2) {
            $this->setFirstName($nameArr[0]);
            $this->setLastName($nameArr[1]);
        } else {
            $this->setLastName($name);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return trim($this->getFirstName() . ' ' . $this->getLastName());
    }

    /**
     * @return string
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception
     */
    public function getTable()
    {
        return $this->getDataMap()->getTableName();
    }

    /**
     * @return string
     */
    public function getObjectName()
    {
        return strtolower(substr(get_class($this),
                strrpos(get_class($this), '\\') ? strrpos(get_class($this), '\\') + 1 : 0)
        );
    }

    /**
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception
     */
    public function getRow()
    {
        $row = [];
        foreach ($this->_getProperties() as $propertyName => $propertyValue) {
            if (in_array($propertyName, ['uid', 'pid'])) {
                $columnName = $propertyName;
            } else {
                $columnMap = $this->getDataMap()->getColumnMap($propertyName);
                if ($columnMap) {
                    $columnName = $columnMap->getColumnName();
                } else {
                    $columnName = null;
                }
            }

            if ($columnName) {
                if (is_object($propertyValue)) {
                    if ($propertyValue instanceof \DateTime) {
                        $row[$columnName] = $propertyValue->getTimestamp();
                    }
                } else {
                    $row[$columnName] = $propertyValue;
                }
            }
        }

        return $row;
    }

    /**
     * @return int
     */
    public function getLocalizedUid()
    {
        return $this->_localizedUid;
    }

    /**
     * @return int
     */
    public function getLanguageUid()
    {
        if ($this->_localizedLanguageUid) {
            return $this->_localizedLanguageUid;
        } else {
            return $this->_languageUid;
        }
    }

    /**
     * @return DataMap
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception
     */
    protected function getDataMap()
    {
        if (!$this->dataMap) {
            /** @var ObjectManager $objectManager */
            $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
            /** @var DataMapper $dataMapper */
            $dataMapper = $objectManager->get(DataMapper::class);
            $this->dataMap = $dataMapper->getDataMap(get_class($this));
        }

        return $this->dataMap;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }
}