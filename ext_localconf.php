<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY,
    'Configuration/Realurl/RealurlExtensionConfiguration.php'));

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'IMIA.' . $_EXTKEY,
    'user_access',
    [
        'User' => 'access, delete, deleteSuccess',
    ],
    [
        'User' => 'access, delete, deleteSuccess',
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'IMIA.' . $_EXTKEY,
    'user_profile',
    [
        'User' => 'profile, delete, deleteSuccess',
    ],
    [
        'User' => 'profile, delete, deleteSuccess',
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'IMIA.' . $_EXTKEY,
    'user_register',
    [
        'User' => 'register, registerSubmit, registerSuccess, confirm, confirmSuccess, activate, activateSuccess',
    ],
    [
        'User' => 'registerSubmit, registerSuccess, confirm, confirmSuccess, activate, activateSuccess',
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'IMIA.' . $_EXTKEY,
    'user_delete',
    [
        'User' => 'delete, deleteSuccess',
    ],
    [
        'User' => 'delete, deleteSuccess',
    ]
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript($_EXTKEY, 'setup',
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TypoScript/setup.ts">', true);

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('powermail')) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\In2code\Powermail\ViewHelpers\Misc\PrefillFieldViewHelper::class] = [
        'className' => \IMIA\ImiaUser\Xclass\Powermail\ViewHelpers\Misc\PrefillFieldViewHelper::class,
    ];
}
