<?php
$EM_CONF[$_EXTKEY] = [
    'title'            => 'IMIA User',
    'description'      => '',
    'category'         => 'plugin',
    'author'           => 'David Frerich',
    'author_email'     => 'd.frerich@imia.de',
    'author_company'   => 'IMIA net based solutions',
    'state'            => 'stable',
    'uploadfolder'     => 0,
    'clearCacheOnLoad' => 1,
    'version'          => '7.0.1',
    'constraints'      => [
        'depends'   => [
            'php'           => '5.5.0-0.0.0',
            'typo3'         => '7.6.0-7.6.999',
            'imia_base_ext' => '7.0.0-0.0.0',
            'imia_composer' => '7.0.0-0.0.0',
        ],
        'conflicts' => [
        ],
        'suggests'  => [
        ],
    ],
];