(function($) {
    if (typeof $ != 'undefined' && $.fn.popover != 'undefined') {
        $(document).ready(function() {
            $('.fieldwrap-password').each(function() {
                var passwordField = $(this).find('input[type="password"]:first');
                passwordField.pwstrength({
                    ui: {
                        container: '#' + $(this).attr('id'),
                        showVerdictsInsideProgressBar: true,
                        viewports: {
                            progress: '.pwstrength_viewport_progress'
                        }
                    }
                });

                var passwordInfo = $(this).find('.password-info:first');
                var passwordTests = passwordInfo.children();
                if (passwordTests.length > 0) {
                    passwordField.keyup(function() {
                        checkPassword();
                    });

                    function checkPassword() {
                        passwordTests.each(function() {
                            var passwordTest = $(this);
                            var passwordTestAll = $('.' + passwordInfo.attr('id') + '-test[data-pattern="' + passwordTest.attr('data-pattern') + '"]');
                            var regex = new RegExp(passwordTest.attr('data-pattern'));
                            if (regex.test(passwordField.val())) {
                                passwordTestAll.removeClass('active').hide();
                            } else {
                                passwordTestAll.addClass('active').show();
                            }

                            $('.popover-content').each(function() {
                                $(this).closest('.popover').show();
                                if ($(this).children(':visible').length == 0) {
                                    $(this).closest('.popover').hide();
                                } else {
                                    if (typeof passwordField.popover != 'undefined') {
                                        passwordField.popover('show');
                                    }
                                }
                            });
                        })
                    }

                    checkPassword();
                }
            });
        });

        $('[data-user-toggle="popover"]').each(function() {
            var field = $(this);
            if (typeof field.popover != 'undefined') {
                field.popover({
                    html: true,
                    content: function() {
                        var popoverContent = $(field.attr('data-html'));
                        if (popoverContent.length > 0) {
                            var content = $('<div></div>').append(popoverContent.clone().children('.active'));
                            return content.html();
                        } else {
                            return '';
                        }
                    }
                });
            }
        });
    }
})(jQuery);