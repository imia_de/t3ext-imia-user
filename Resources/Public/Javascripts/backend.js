(function($){
    $(document).ready(function(){
        $('#start-import').click(function(e){
            e.preventDefault();

            $(this).closest('form')
                .attr('action', $(this).attr('data-action'))
                .submit();

            return false;
        });
        $('.special-mapping-field').change(function(){
            if ($(this).val() == '#assignfixed#') {
                $(this).next('.fixed-value').show();
            } else {
                $(this).next('.fixed-value').hide();
            }
        });
    });
})(TYPO3.jQuery);