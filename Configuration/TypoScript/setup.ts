plugin.tx_imiauser {
    settings {
        duplicateOverwrite = 0
        emailAsUsername = 0
        passwordRepeat = 1
        passwordGenerate = 0
        passwordGenerateForceInMail = 0
        passwordValidate {
            minLength = 6
            specialCharacter = 0
            # 0 = Optional, 1 = Required, 2 = Prohibited
            lowerUpperCase = 0
            # 0 = Optional, 1 = Required, 2 = Only lowercase, 3 = Only uppercase
            number = 0
            # 0 = Optional, 1 = Required, 2 = Prohibited
            compareFields = username,email,firstName,lastName
        }
        passwordInfo = 0
        # jQuery and Bootstrap 3.x required
        passwordStrength = 0
        # jQuery and Bootstrap 3.x required

        defaultLanguage = 0
        defaultLanguageFlag = en
        activation = 0
        gtc = 0
        gtcOptional = 0
        gtcInProfile = 0
        gtcEditable = 0

        registerSuccess = 1
        confirmSuccess = 1
        activateSuccess = 1
        deleteSuccess = 1

        login.permantent = 1

        captcha = 0
        captcha {
            publicKey = 6LdhZukSAAAAAAwXIKziXugQn7sqxvxNfUbr2v4Z
            privateKey = 6LdhZukSAAAAAI0CKW2MtEJULHVF4b_K4NTKIgI4
            theme = clean
        }

        fields {
            enabled = firstName, lastName, gender, address, zip, city, country, email, telephone
            mandatory = firstName, lastName, gender
        }

        file.storageUid = 1
    }
}

page.includeJSFooter {
    z99_pwstrength = EXT:imia_user/Resources/Public/Javascripts/pwstrength-bootstrap.min.js
    z99_userfrontend = EXT:imia_user/Resources/Public/Javascripts/frontend.js
}