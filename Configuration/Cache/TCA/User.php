<?php
if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}

$TCA['fe_users'] = [
    'ctrl' => $TCA['fe_users']['ctrl'],
    'interface' => [
        'showRecordFieldList' => 'first_name, last_name, country, image, gender, crdate, tstamp, position, mobile, confirmed, language, pdf, description, gtc, gtc_time, register_checks',
    ],
    'types' => [
        0 => [
            'showitem' => 'username, password, lastlogin, --palette--;;gtc, register_checks, --div--;LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_tab_access, disable, usergroup, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:fe_users.tabs.personelData, --palette--;LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_name;name, --palette--;;company, --palette--;;address, --palette--;LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_contact;contact, --div--;LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_tab_description, description, image, pdf',
        ],
    ],
    'palettes' => [
        'name' => [
            'showitem' => 'gender, --linebreak--, first_name, last_name',
            'canNotCollapse' => true,
        ],
        'company' => [
            'showitem' => 'company, --linebreak--, position',
            'canNotCollapse' => true,
        ],
        'address' => [
            'showitem' => 'address, --linebreak--, zip, city, --linebreak--, country',
            'canNotCollapse' => true,
        ],
        'contact' => [
            'showitem' => 'telephone, --linebreak--, mobile, --linebreak--, fax, --linebreak--, email',
            'canNotCollapse' => true,
        ],
        'gtc' => [
            'showitem' => 'gtc, --linebreak--, gtc_time',
            'canNotCollapse' => true,
        ],
    ],
    'columns' => [
        'pid' => [
            'label' => '[pid]',
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'first_name' => [
            'label' => '[user_firstname]',
            'config' => [
                'size' => '12',
                'eval' => 'trim',
                'type' => 'input',
            ],
        ],
        'last_name' => [
            'label' => '[user_lastname]',
            'config' => [
                'size' => '12',
                'eval' => 'trim',
                'type' => 'input',
            ],
        ],
        'country' => [
            'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_country',
            'l10n_mode' => 'exclude',
            'config' => [
                'items' => [
                    0 => [
                        0 => '',
                        1 => 0,
                    ],
                ],
                'itemsProcFunc' => 'IMIA\ImiaUser\User\TCA\User->country',
                'foreign_table' => 'static_countries',
                'size' => '1',
                'renderType' => 'selectSingle',
                'maxitems' => '1',
                'minitems' => '0',
                'type' => 'select',
                'default' => '54',
            ],
        ],
        'image' => [
            'label' => '[user_image]',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
                'appearance' => [
                    'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference',
                    'headerThumbnail' => [
                        'width' => '64m',
                        'height' => '64m',
                    ],
                ],
                'foreign_types' => [
                    0 => [
                        'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                    ],
                    1 => [
                        'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                    ],
                    2 => [
                        'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                    ],
                    3 => [
                        'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                    ],
                    4 => [
                        'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                    ],
                    5 => [
                        'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                    ],
                ],
                'maxitems' => '1',
            ], 'gif,jpg,jpeg,tif,tiff,bmp,pcx,tga,png,pdf,ai,svg,webp'),
        ],
        'gender' => [
            'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender',
            'l10n_mode' => 'exclude',
            'config' => [
                'items' => [
                    0 => [
                        0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender_none',
                        1 => '',
                    ],
                    1 => [
                        0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender_f',
                        1 => 'f',
                    ],
                    2 => [
                        0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender_m',
                        1 => 'm',
                    ],
                ],
                'renderType' => 'selectSingle',
                'type' => 'select',
                'default' => '',
            ],
        ],
        'crdate' => [
            'label' => '[crdate]',
            'l10n_mode' => 'exclude',
            'config' => [
                'size' => '13',
                'max' => '20',
                'eval' => 'datetime',
                'checkbox' => '0',
                'type' => 'input',
                'default' => '0',
            ],
        ],
        'tstamp' => [
            'label' => '[tstamp]',
            'l10n_mode' => 'exclude',
            'config' => [
                'size' => '13',
                'max' => '20',
                'eval' => 'datetime',
                'checkbox' => '0',
                'type' => 'input',
                'default' => '0',
            ],
        ],
        'position' => [
            'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_position',
            'config' => [
                'eval' => 'trim',
                'type' => 'input',
            ],
        ],
        'mobile' => [
            'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_mobile',
            'config' => [
                'eval' => 'trim',
                'type' => 'input',
            ],
        ],
        'confirmed' => [
            'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_confirmed',
            'l10n_mode' => 'exclude',
            'config' => [
                'type' => 'check',
            ],
        ],
        'confirm_token' => [
            'label' => '[user_confirmtoken]',
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'activate_token' => [
            'label' => '[user_activatetoken]',
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'language' => [
            'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_language',
            'l10n_mode' => 'exclude',
            'config' => [
                'items' => [
                    0 => [
                        0 => '',
                        1 => '',
                    ],
                ],
                'foreign_table' => 'sys_languages',
                'renderType' => 'selectSingle',
                'type' => 'select',
            ],
        ],
        'pdf' => [
            'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_pdf',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('pdf', [
                'foreign_types' => [
                    0 => [
                        'showitem' => 'title, --palette--;;filePalette',
                    ],
                    1 => [
                        'showitem' => 'title, --palette--;;filePalette',
                    ],
                    2 => [
                        'showitem' => 'title, --palette--;;filePalette',
                    ],
                    3 => [
                        'showitem' => 'title, --palette--;;filePalette',
                    ],
                    4 => [
                        'showitem' => 'title, --palette--;;filePalette',
                    ],
                    5 => [
                        'showitem' => 'title, --palette--;;filePalette',
                    ],
                ],
                'maxitems' => '1',
            ], 'pdf'),
        ],
        'description' => [
            'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_description',
            'config' => [
                'cols' => '40',
                'rows' => '5',
                'type' => 'text',
            ],
        ],
        'gtc' => [
            'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gtc',
            'l10n_mode' => 'exclude',
            'config' => [
                'items' => [
                    0 => [
                        0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gtc_check',
                    ],
                ],
                'type' => 'check',
                'readOnly' => '1',
            ],
        ],
        'gtc_time' => [
            'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gtctime',
            'l10n_mode' => 'exclude',
            'displayCond' => 'FIELD:gtc_time:REQ:true',
            'config' => [
                'size' => '13',
                'max' => '20',
                'eval' => 'datetime',
                'checkbox' => '0',
                'type' => 'input',
                'default' => '0',
                'readOnly' => '1',
            ],
        ],
        'register_checks' => [
            'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_registerchecks',
            'l10n_mode' => 'exclude',
            'displayCond' => 'FIELD:register_checks:REQ:true',
            'config' => [
                'userFunc' => 'IMIA\ImiaUser\User\TCA\User->registerChecks',
                'type' => 'user',
            ],
        ],
   ],
];