<?php
if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::loadNewTcaColumnsConfigFiles();

// ******************************************************************
// TCA for IMIA\ImiaUser\Domain\Model\Country
// (static_countries)
// ******************************************************************
if (array_key_exists('static_countries', $GLOBALS['TCA'])) {
    $GLOBALS['TCA']['static_countries']['ctrl']['label'] = 'cn_short_local';
    $GLOBALS['TCA']['static_countries']['ctrl']['default_sortby'] = 'ORDER BY cn_short_local';
    $GLOBALS['TCA']['static_countries']['ctrl']['hideAtCopy'] = true;
    $GLOBALS['TCA']['static_countries']['ctrl']['prependAtCopy'] = 'LLL:EXT:lang/locallang_general.xlf:LGL.prependAtCopy';
    $GLOBALS['TCA']['static_countries']['columns']['cn_iso_2']['mapOnProperty'] = "iso2";
    $GLOBALS['TCA']['static_countries']['columns']['cn_iso_3']['mapOnProperty'] = "iso3";
    $GLOBALS['TCA']['static_countries']['columns']['cn_iso_nr']['mapOnProperty'] = "isoNr";
    $GLOBALS['TCA']['static_countries']['columns']['cn_parent_tr_iso_nr']['mapOnProperty'] = "parentTrIsoNr";
    $GLOBALS['TCA']['static_countries']['columns']['cn_official_name_local']['mapOnProperty'] = "officialNameLocal";
    $GLOBALS['TCA']['static_countries']['columns']['cn_official_name_en']['mapOnProperty'] = "officialNameEn";
    $GLOBALS['TCA']['static_countries']['columns']['cn_capital']['mapOnProperty'] = "capital";
    $GLOBALS['TCA']['static_countries']['columns']['cn_tldomain']['mapOnProperty'] = "tldomain";
    $GLOBALS['TCA']['static_countries']['columns']['cn_currency_iso_3']['mapOnProperty'] = "currencyIso3";
    $GLOBALS['TCA']['static_countries']['columns']['cn_currency_iso_nr']['mapOnProperty'] = "currencyIsoNr";
    $GLOBALS['TCA']['static_countries']['columns']['cn_address_format']['mapOnProperty'] = "addressFormat";
    $GLOBALS['TCA']['static_countries']['columns']['cn_zone_flag']['mapOnProperty'] = "zoneFlag";
    $GLOBALS['TCA']['static_countries']['columns']['cn_short_local']['mapOnProperty'] = "shortLocal";
    $GLOBALS['TCA']['static_countries']['columns']['cn_short_en']['mapOnProperty'] = "shortEn";
    $GLOBALS['TCA']['static_countries']['columns']['cn_short_de']['mapOnProperty'] = "shortDe";
    $GLOBALS['TCA']['static_countries']['columns']['cn_short_fr']['mapOnProperty'] = "shortFr";
    $GLOBALS['TCA']['static_countries']['columns']['cn_short_it']['mapOnProperty'] = "shortIt";
    $GLOBALS['TCA']['static_countries']['columns']['cn_short_cz']['mapOnProperty'] = "shortCz";
    $GLOBALS['TCA']['static_countries']['columns']['cn_short_pl']['mapOnProperty'] = "shortPl";
    $GLOBALS['TCA']['static_countries']['columns']['cn_short_pt']['mapOnProperty'] = "shortPt";
    $GLOBALS['TCA']['static_countries']['columns']['cn_short_ro']['mapOnProperty'] = "shortRo";
    $GLOBALS['TCA']['static_countries']['columns']['cn_short_zh']['mapOnProperty'] = "shortZh";
    $GLOBALS['TCA']['static_countries']['columns']['cn_short_es']['mapOnProperty'] = "shortEs";
    $GLOBALS['TCA']['static_countries']['columns']['cn_short_da']['mapOnProperty'] = "shortDa";
    $GLOBALS['TCA']['static_countries']['columns']['cn_uno_member']['mapOnProperty'] = "unoMember";
} else {
    $GLOBALS['TCA']['static_countries'] = [
        'ctrl' => [
            'title' => '[staticcountries]',
            'label' => 'cn_short_local',
            'dividers2tabs' => true,
            'default_sortby' => 'ORDER BY cn_short_local',
            'hideAtCopy' => true,
            'prependAtCopy' => 'LLL:EXT:lang/locallang_general.xlf:LGL.prependAtCopy',
            'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('imia_user', 'Configuration/Cache/TCA/Country.php'),
        ]
    ];
}
// ******************************************************************
// TCA for IMIA\ImiaUser\Domain\Model\User
// (fe_users)
// ******************************************************************
if (array_key_exists('fe_users', $GLOBALS['TCA'])) {
    $GLOBALS['TCA']['fe_users']['ctrl']['hideAtCopy'] = true;
    $GLOBALS['TCA']['fe_users']['ctrl']['prependAtCopy'] = 'LLL:EXT:lang/locallang_general.xlf:LGL.prependAtCopy';
    if (array_key_exists('types', $GLOBALS['TCA']['fe_users'])) {
        if (array_key_exists('0', $GLOBALS['TCA']['fe_users']['types'])) {
            $GLOBALS['TCA']['fe_users']['types']['0']['showitem'] = 'username, password, lastlogin, --palette--;;gtc, register_checks, --div--;LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_tab_access, disable, usergroup, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:fe_users.tabs.personelData, --palette--;LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_name;name, --palette--;;company, --palette--;;address, --palette--;LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_contact;contact, --div--;LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_tab_description, description, image, pdf';
        } else {
            $GLOBALS['TCA']['fe_users']['types']['0'] = [
                'showitem' => 'username, password, lastlogin, --palette--;;gtc, register_checks, --div--;LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_tab_access, disable, usergroup, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:fe_users.tabs.personelData, --palette--;LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_name;name, --palette--;;company, --palette--;;address, --palette--;LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_contact;contact, --div--;LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_tab_description, description, image, pdf',
            ];
        }
    } else {
        $GLOBALS['TCA']['fe_users']['types'] = [
                0 => [
                    'showitem' => 'username, password, lastlogin, --palette--;;gtc, register_checks, --div--;LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_tab_access, disable, usergroup, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:fe_users.tabs.personelData, --palette--;LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_name;name, --palette--;;company, --palette--;;address, --palette--;LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_contact;contact, --div--;LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_tab_description, description, image, pdf',
                ],
            ];
    }
    if (array_key_exists('palettes', $GLOBALS['TCA']['fe_users'])) {
        if (array_key_exists('name', $GLOBALS['TCA']['fe_users']['palettes'])) {
            $GLOBALS['TCA']['fe_users']['palettes']['name']['showitem'] = 'gender, --linebreak--, first_name, last_name';
            $GLOBALS['TCA']['fe_users']['palettes']['name']['canNotCollapse'] = true;
        } else {
            $GLOBALS['TCA']['fe_users']['palettes']['name'] = [
                'showitem' => 'gender, --linebreak--, first_name, last_name',
                'canNotCollapse' => true,
            ];
        }
        if (array_key_exists('company', $GLOBALS['TCA']['fe_users']['palettes'])) {
            $GLOBALS['TCA']['fe_users']['palettes']['company']['showitem'] = 'company, --linebreak--, position';
            $GLOBALS['TCA']['fe_users']['palettes']['company']['canNotCollapse'] = true;
        } else {
            $GLOBALS['TCA']['fe_users']['palettes']['company'] = [
                'showitem' => 'company, --linebreak--, position',
                'canNotCollapse' => true,
            ];
        }
        if (array_key_exists('address', $GLOBALS['TCA']['fe_users']['palettes'])) {
            $GLOBALS['TCA']['fe_users']['palettes']['address']['showitem'] = 'address, --linebreak--, zip, city, --linebreak--, country';
            $GLOBALS['TCA']['fe_users']['palettes']['address']['canNotCollapse'] = true;
        } else {
            $GLOBALS['TCA']['fe_users']['palettes']['address'] = [
                'showitem' => 'address, --linebreak--, zip, city, --linebreak--, country',
                'canNotCollapse' => true,
            ];
        }
        if (array_key_exists('contact', $GLOBALS['TCA']['fe_users']['palettes'])) {
            $GLOBALS['TCA']['fe_users']['palettes']['contact']['showitem'] = 'telephone, --linebreak--, mobile, --linebreak--, fax, --linebreak--, email';
            $GLOBALS['TCA']['fe_users']['palettes']['contact']['canNotCollapse'] = true;
        } else {
            $GLOBALS['TCA']['fe_users']['palettes']['contact'] = [
                'showitem' => 'telephone, --linebreak--, mobile, --linebreak--, fax, --linebreak--, email',
                'canNotCollapse' => true,
            ];
        }
        if (array_key_exists('gtc', $GLOBALS['TCA']['fe_users']['palettes'])) {
            $GLOBALS['TCA']['fe_users']['palettes']['gtc']['showitem'] = 'gtc, --linebreak--, gtc_time';
            $GLOBALS['TCA']['fe_users']['palettes']['gtc']['canNotCollapse'] = true;
        } else {
            $GLOBALS['TCA']['fe_users']['palettes']['gtc'] = [
                'showitem' => 'gtc, --linebreak--, gtc_time',
                'canNotCollapse' => true,
            ];
        }
    } else {
        $GLOBALS['TCA']['fe_users']['palettes'] = [
                'name' => [
                    'showitem' => 'gender, --linebreak--, first_name, last_name',
                    'canNotCollapse' => true,
                ],
                'company' => [
                    'showitem' => 'company, --linebreak--, position',
                    'canNotCollapse' => true,
                ],
                'address' => [
                    'showitem' => 'address, --linebreak--, zip, city, --linebreak--, country',
                    'canNotCollapse' => true,
                ],
                'contact' => [
                    'showitem' => 'telephone, --linebreak--, mobile, --linebreak--, fax, --linebreak--, email',
                    'canNotCollapse' => true,
                ],
                'gtc' => [
                    'showitem' => 'gtc, --linebreak--, gtc_time',
                    'canNotCollapse' => true,
                ],
            ];
    }
    if (array_key_exists('pid', $GLOBALS['TCA']['fe_users']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['fe_users']['columns']['pid'])) {
            $GLOBALS['TCA']['fe_users']['columns']['pid']['config']['type'] = 'passthrough';
        } else {
            $GLOBALS['TCA']['fe_users']['columns']['pid']['config'] = [
                'type' => 'passthrough',
            ];
        }
    } else {
        $tempColumn = [
            'pid' => [
                'label' => '[pid]',
                'config' => [
                    'type' => 'passthrough',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
    if (array_key_exists('first_name', $GLOBALS['TCA']['fe_users']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['fe_users']['columns']['first_name'])) {
            $GLOBALS['TCA']['fe_users']['columns']['first_name']['config']['size'] = '12';
            $GLOBALS['TCA']['fe_users']['columns']['first_name']['config']['type'] = 'input';
        } else {
            $GLOBALS['TCA']['fe_users']['columns']['first_name']['config'] = [
                'size' => '12',
                'type' => 'input',
            ];
        }
    } else {
        $tempColumn = [
            'first_name' => [
                'label' => '[user_firstname]',
                'config' => [
                    'size' => '12',
                    'eval' => 'trim',
                    'type' => 'input',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
    if (array_key_exists('last_name', $GLOBALS['TCA']['fe_users']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['fe_users']['columns']['last_name'])) {
            $GLOBALS['TCA']['fe_users']['columns']['last_name']['config']['size'] = '12';
            $GLOBALS['TCA']['fe_users']['columns']['last_name']['config']['type'] = 'input';
        } else {
            $GLOBALS['TCA']['fe_users']['columns']['last_name']['config'] = [
                'size' => '12',
                'type' => 'input',
            ];
        }
    } else {
        $tempColumn = [
            'last_name' => [
                'label' => '[user_lastname]',
                'config' => [
                    'size' => '12',
                    'eval' => 'trim',
                    'type' => 'input',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
    if (array_key_exists('country', $GLOBALS['TCA']['fe_users']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['fe_users']['columns']['country'])) {
            if (array_key_exists('items', $GLOBALS['TCA']['fe_users']['columns']['country']['config'])) {
                if (array_key_exists('0', $GLOBALS['TCA']['fe_users']['columns']['country']['config']['items'])) {
                    $GLOBALS['TCA']['fe_users']['columns']['country']['config']['items']['0']['0'] = '';
                    $GLOBALS['TCA']['fe_users']['columns']['country']['config']['items']['0']['1'] = 0;
                } else {
                    $GLOBALS['TCA']['fe_users']['columns']['country']['config']['items']['0'] = [
                0 => '',
                1 => 0,
            ];
                }
            } else {
                $GLOBALS['TCA']['fe_users']['columns']['country']['config']['items'] = [
                0 => [
                    0 => '',
                    1 => 0,
                ],
            ];
            }
            $GLOBALS['TCA']['fe_users']['columns']['country']['config']['itemsProcFunc'] = 'IMIA\ImiaUser\User\TCA\User->country';
            $GLOBALS['TCA']['fe_users']['columns']['country']['config']['foreign_table'] = 'static_countries';
            $GLOBALS['TCA']['fe_users']['columns']['country']['config']['size'] = '1';
            $GLOBALS['TCA']['fe_users']['columns']['country']['config']['maxitems'] = '1';
            $GLOBALS['TCA']['fe_users']['columns']['country']['config']['minitems'] = '0';
            $GLOBALS['TCA']['fe_users']['columns']['country']['config']['type'] = 'select';
            $GLOBALS['TCA']['fe_users']['columns']['country']['config']['default'] = '54';
        } else {
            $GLOBALS['TCA']['fe_users']['columns']['country']['config'] = [
                'items' => [
                    0 => [
                        0 => '',
                        1 => 0,
                    ],
                ],
                'itemsProcFunc' => 'IMIA\ImiaUser\User\TCA\User->country',
                'foreign_table' => 'static_countries',
                'size' => '1',
                'maxitems' => '1',
                'minitems' => '0',
                'type' => 'select',
                'default' => '54',
            ];
        }
    } else {
        $tempColumn = [
            'country' => [
                'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_country',
                'l10n_mode' => 'exclude',
                'config' => [
                    'items' => [
                        0 => [
                            0 => '',
                            1 => 0,
                        ],
                    ],
                    'itemsProcFunc' => 'IMIA\ImiaUser\User\TCA\User->country',
                    'foreign_table' => 'static_countries',
                    'size' => '1',
                    'renderType' => 'selectSingle',
                    'maxitems' => '1',
                    'minitems' => '0',
                    'type' => 'select',
                    'default' => '54',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
    if (array_key_exists('image', $GLOBALS['TCA']['fe_users']['columns'])) {
        $GLOBALS['TCA']['fe_users']['columns']['image']['config'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
            'maxitems' => '1',
        ]);
    } else {
        $tempColumn = [
            'image' => [
                'label' => '[user_image]',
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference',
                        'headerThumbnail' => [
                            'width' => '64m',
                            'height' => '64m',
                        ],
                    ],
                    'foreign_types' => [
                        0 => [
                            'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        1 => [
                            'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        2 => [
                            'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        3 => [
                            'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        4 => [
                            'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        5 => [
                            'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                    ],
                    'maxitems' => '1',
                ], 'gif,jpg,jpeg,tif,tiff,bmp,pcx,tga,png,pdf,ai,svg,webp'),
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
    if (array_key_exists('gender', $GLOBALS['TCA']['fe_users']['columns'])) {
        $GLOBALS['TCA']['fe_users']['columns']['gender']['label'] = 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender';
        if (array_key_exists('config', $GLOBALS['TCA']['fe_users']['columns']['gender'])) {
            if (array_key_exists('items', $GLOBALS['TCA']['fe_users']['columns']['gender']['config'])) {
                if (array_key_exists('0', $GLOBALS['TCA']['fe_users']['columns']['gender']['config']['items'])) {
                    $GLOBALS['TCA']['fe_users']['columns']['gender']['config']['items']['0']['0'] = 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender_none';
                    $GLOBALS['TCA']['fe_users']['columns']['gender']['config']['items']['0']['1'] = '';
                } else {
                    $GLOBALS['TCA']['fe_users']['columns']['gender']['config']['items']['0'] = [
                0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender_none',
                1 => '',
            ];
                }
                if (array_key_exists('1', $GLOBALS['TCA']['fe_users']['columns']['gender']['config']['items'])) {
                    $GLOBALS['TCA']['fe_users']['columns']['gender']['config']['items']['1']['0'] = 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender_f';
                    $GLOBALS['TCA']['fe_users']['columns']['gender']['config']['items']['1']['1'] = 'f';
                } else {
                    $GLOBALS['TCA']['fe_users']['columns']['gender']['config']['items']['1'] = [
                0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender_f',
                1 => 'f',
            ];
                }
                if (array_key_exists('2', $GLOBALS['TCA']['fe_users']['columns']['gender']['config']['items'])) {
                    $GLOBALS['TCA']['fe_users']['columns']['gender']['config']['items']['2']['0'] = 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender_m';
                    $GLOBALS['TCA']['fe_users']['columns']['gender']['config']['items']['2']['1'] = 'm';
                } else {
                    $GLOBALS['TCA']['fe_users']['columns']['gender']['config']['items']['2'] = [
                0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender_m',
                1 => 'm',
            ];
                }
            } else {
                $GLOBALS['TCA']['fe_users']['columns']['gender']['config']['items'] = [
                0 => [
                    0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender_none',
                    1 => '',
                ],
                1 => [
                    0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender_f',
                    1 => 'f',
                ],
                2 => [
                    0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender_m',
                    1 => 'm',
                ],
            ];
            }
            $GLOBALS['TCA']['fe_users']['columns']['gender']['config']['type'] = 'select';
            $GLOBALS['TCA']['fe_users']['columns']['gender']['config']['default'] = '';
        } else {
            $GLOBALS['TCA']['fe_users']['columns']['gender']['config'] = [
                'items' => [
                    0 => [
                        0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender_none',
                        1 => '',
                    ],
                    1 => [
                        0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender_f',
                        1 => 'f',
                    ],
                    2 => [
                        0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender_m',
                        1 => 'm',
                    ],
                ],
                'type' => 'select',
                'default' => '',
            ];
        }
    } else {
        $tempColumn = [
            'gender' => [
                'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender',
                'l10n_mode' => 'exclude',
                'config' => [
                    'items' => [
                        0 => [
                            0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender_none',
                            1 => '',
                        ],
                        1 => [
                            0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender_f',
                            1 => 'f',
                        ],
                        2 => [
                            0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gender_m',
                            1 => 'm',
                        ],
                    ],
                    'renderType' => 'selectSingle',
                    'type' => 'select',
                    'default' => '',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
    if (array_key_exists('crdate', $GLOBALS['TCA']['fe_users']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['fe_users']['columns']['crdate'])) {
            $GLOBALS['TCA']['fe_users']['columns']['crdate']['config']['type'] = 'input';
        } else {
            $GLOBALS['TCA']['fe_users']['columns']['crdate']['config'] = [
                'type' => 'input',
            ];
        }
    } else {
        $tempColumn = [
            'crdate' => [
                'label' => '[crdate]',
                'l10n_mode' => 'exclude',
                'config' => [
                    'size' => '13',
                    'max' => '20',
                    'eval' => 'datetime',
                    'checkbox' => '0',
                    'type' => 'input',
                    'default' => '0',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
    if (array_key_exists('tstamp', $GLOBALS['TCA']['fe_users']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['fe_users']['columns']['tstamp'])) {
            $GLOBALS['TCA']['fe_users']['columns']['tstamp']['config']['type'] = 'input';
        } else {
            $GLOBALS['TCA']['fe_users']['columns']['tstamp']['config'] = [
                'type' => 'input',
            ];
        }
    } else {
        $tempColumn = [
            'tstamp' => [
                'label' => '[tstamp]',
                'l10n_mode' => 'exclude',
                'config' => [
                    'size' => '13',
                    'max' => '20',
                    'eval' => 'datetime',
                    'checkbox' => '0',
                    'type' => 'input',
                    'default' => '0',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
    if (array_key_exists('position', $GLOBALS['TCA']['fe_users']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['fe_users']['columns']['position'])) {
            $GLOBALS['TCA']['fe_users']['columns']['position']['config']['type'] = 'input';
        } else {
            $GLOBALS['TCA']['fe_users']['columns']['position']['config'] = [
                'type' => 'input',
            ];
        }
    } else {
        $tempColumn = [
            'position' => [
                'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_position',
                'config' => [
                    'eval' => 'trim',
                    'type' => 'input',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
    if (array_key_exists('mobile', $GLOBALS['TCA']['fe_users']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['fe_users']['columns']['mobile'])) {
            $GLOBALS['TCA']['fe_users']['columns']['mobile']['config']['type'] = 'input';
        } else {
            $GLOBALS['TCA']['fe_users']['columns']['mobile']['config'] = [
                'type' => 'input',
            ];
        }
    } else {
        $tempColumn = [
            'mobile' => [
                'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_mobile',
                'config' => [
                    'eval' => 'trim',
                    'type' => 'input',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
    if (array_key_exists('confirmed', $GLOBALS['TCA']['fe_users']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['fe_users']['columns']['confirmed'])) {
            $GLOBALS['TCA']['fe_users']['columns']['confirmed']['config']['type'] = 'check';
        } else {
            $GLOBALS['TCA']['fe_users']['columns']['confirmed']['config'] = [
                'type' => 'check',
            ];
        }
    } else {
        $tempColumn = [
            'confirmed' => [
                'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_confirmed',
                'l10n_mode' => 'exclude',
                'config' => [
                    'type' => 'check',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
    if (array_key_exists('confirm_token', $GLOBALS['TCA']['fe_users']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['fe_users']['columns']['confirm_token'])) {
            $GLOBALS['TCA']['fe_users']['columns']['confirm_token']['config']['type'] = 'passthrough';
        } else {
            $GLOBALS['TCA']['fe_users']['columns']['confirm_token']['config'] = [
                'type' => 'passthrough',
            ];
        }
    } else {
        $tempColumn = [
            'confirm_token' => [
                'label' => '[user_confirmtoken]',
                'config' => [
                    'type' => 'passthrough',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
    if (array_key_exists('activate_token', $GLOBALS['TCA']['fe_users']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['fe_users']['columns']['activate_token'])) {
            $GLOBALS['TCA']['fe_users']['columns']['activate_token']['config']['type'] = 'passthrough';
        } else {
            $GLOBALS['TCA']['fe_users']['columns']['activate_token']['config'] = [
                'type' => 'passthrough',
            ];
        }
    } else {
        $tempColumn = [
            'activate_token' => [
                'label' => '[user_activatetoken]',
                'config' => [
                    'type' => 'passthrough',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
    if (array_key_exists('language', $GLOBALS['TCA']['fe_users']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['fe_users']['columns']['language'])) {
            if (array_key_exists('items', $GLOBALS['TCA']['fe_users']['columns']['language']['config'])) {
                if (array_key_exists('0', $GLOBALS['TCA']['fe_users']['columns']['language']['config']['items'])) {
                    $GLOBALS['TCA']['fe_users']['columns']['language']['config']['items']['0']['0'] = '';
                    $GLOBALS['TCA']['fe_users']['columns']['language']['config']['items']['0']['1'] = '';
                } else {
                    $GLOBALS['TCA']['fe_users']['columns']['language']['config']['items']['0'] = [
                0 => '',
                1 => '',
            ];
                }
            } else {
                $GLOBALS['TCA']['fe_users']['columns']['language']['config']['items'] = [
                0 => [
                    0 => '',
                    1 => '',
                ],
            ];
            }
            $GLOBALS['TCA']['fe_users']['columns']['language']['config']['foreign_table'] = 'sys_languages';
            $GLOBALS['TCA']['fe_users']['columns']['language']['config']['type'] = 'select';
        } else {
            $GLOBALS['TCA']['fe_users']['columns']['language']['config'] = [
                'items' => [
                    0 => [
                        0 => '',
                        1 => '',
                    ],
                ],
                'foreign_table' => 'sys_languages',
                'type' => 'select',
            ];
        }
    } else {
        $tempColumn = [
            'language' => [
                'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_language',
                'l10n_mode' => 'exclude',
                'config' => [
                    'items' => [
                        0 => [
                            0 => '',
                            1 => '',
                        ],
                    ],
                    'foreign_table' => 'sys_languages',
                    'renderType' => 'selectSingle',
                    'type' => 'select',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
    if (array_key_exists('pdf', $GLOBALS['TCA']['fe_users']['columns'])) {
        $GLOBALS['TCA']['fe_users']['columns']['pdf']['config'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('pdf', [
            'maxitems' => '1',
        ]);
    } else {
        $tempColumn = [
            'pdf' => [
                'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_pdf',
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('pdf', [
                    'foreign_types' => [
                        0 => [
                            'showitem' => 'title, --palette--;;filePalette',
                        ],
                        1 => [
                            'showitem' => 'title, --palette--;;filePalette',
                        ],
                        2 => [
                            'showitem' => 'title, --palette--;;filePalette',
                        ],
                        3 => [
                            'showitem' => 'title, --palette--;;filePalette',
                        ],
                        4 => [
                            'showitem' => 'title, --palette--;;filePalette',
                        ],
                        5 => [
                            'showitem' => 'title, --palette--;;filePalette',
                        ],
                    ],
                    'maxitems' => '1',
                ], 'pdf'),
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
    if (array_key_exists('description', $GLOBALS['TCA']['fe_users']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['fe_users']['columns']['description'])) {
            $GLOBALS['TCA']['fe_users']['columns']['description']['config']['type'] = 'text';
        } else {
            $GLOBALS['TCA']['fe_users']['columns']['description']['config'] = [
                'type' => 'text',
            ];
        }
    } else {
        $tempColumn = [
            'description' => [
                'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_description',
                'config' => [
                    'cols' => '40',
                    'rows' => '5',
                    'type' => 'text',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
    if (array_key_exists('gtc', $GLOBALS['TCA']['fe_users']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['fe_users']['columns']['gtc'])) {
            if (array_key_exists('items', $GLOBALS['TCA']['fe_users']['columns']['gtc']['config'])) {
                if (array_key_exists('0', $GLOBALS['TCA']['fe_users']['columns']['gtc']['config']['items'])) {
                    $GLOBALS['TCA']['fe_users']['columns']['gtc']['config']['items']['0']['0'] = 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gtc_check';
                } else {
                    $GLOBALS['TCA']['fe_users']['columns']['gtc']['config']['items']['0'] = [
                0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gtc_check',
            ];
                }
            } else {
                $GLOBALS['TCA']['fe_users']['columns']['gtc']['config']['items'] = [
                0 => [
                    0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gtc_check',
                ],
            ];
            }
            $GLOBALS['TCA']['fe_users']['columns']['gtc']['config']['type'] = 'check';
            $GLOBALS['TCA']['fe_users']['columns']['gtc']['config']['readOnly'] = '1';
        } else {
            $GLOBALS['TCA']['fe_users']['columns']['gtc']['config'] = [
                'items' => [
                    0 => [
                        0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gtc_check',
                    ],
                ],
                'type' => 'check',
                'readOnly' => '1',
            ];
        }
    } else {
        $tempColumn = [
            'gtc' => [
                'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gtc',
                'l10n_mode' => 'exclude',
                'config' => [
                    'items' => [
                        0 => [
                            0 => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gtc_check',
                        ],
                    ],
                    'type' => 'check',
                    'readOnly' => '1',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
    if (array_key_exists('gtc_time', $GLOBALS['TCA']['fe_users']['columns'])) {
        $GLOBALS['TCA']['fe_users']['columns']['gtc_time']['displayCond'] = 'FIELD:gtc_time:REQ:true';
        if (array_key_exists('config', $GLOBALS['TCA']['fe_users']['columns']['gtc_time'])) {
            $GLOBALS['TCA']['fe_users']['columns']['gtc_time']['config']['type'] = 'input';
            $GLOBALS['TCA']['fe_users']['columns']['gtc_time']['config']['readOnly'] = '1';
        } else {
            $GLOBALS['TCA']['fe_users']['columns']['gtc_time']['config'] = [
                'type' => 'input',
                'readOnly' => '1',
            ];
        }
    } else {
        $tempColumn = [
            'gtc_time' => [
                'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_gtctime',
                'l10n_mode' => 'exclude',
                'displayCond' => 'FIELD:gtc_time:REQ:true',
                'config' => [
                    'size' => '13',
                    'max' => '20',
                    'eval' => 'datetime',
                    'checkbox' => '0',
                    'type' => 'input',
                    'default' => '0',
                    'readOnly' => '1',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
    if (array_key_exists('register_checks', $GLOBALS['TCA']['fe_users']['columns'])) {
        $GLOBALS['TCA']['fe_users']['columns']['register_checks']['displayCond'] = 'FIELD:register_checks:REQ:true';
        if (array_key_exists('config', $GLOBALS['TCA']['fe_users']['columns']['register_checks'])) {
            $GLOBALS['TCA']['fe_users']['columns']['register_checks']['config']['userFunc'] = 'IMIA\ImiaUser\User\TCA\User->registerChecks';
            $GLOBALS['TCA']['fe_users']['columns']['register_checks']['config']['type'] = 'user';
        } else {
            $GLOBALS['TCA']['fe_users']['columns']['register_checks']['config'] = [
                'userFunc' => 'IMIA\ImiaUser\User\TCA\User->registerChecks',
                'type' => 'user',
            ];
        }
    } else {
        $tempColumn = [
            'register_checks' => [
                'label' => 'LLL:EXT:imia_user/Resources/Private/Language/DB.xlf:user_registerchecks',
                'l10n_mode' => 'exclude',
                'displayCond' => 'FIELD:register_checks:REQ:true',
                'config' => [
                    'userFunc' => 'IMIA\ImiaUser\User\TCA\User->registerChecks',
                    'type' => 'user',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumn);
    }
} else {
    $GLOBALS['TCA']['fe_users'] = [
        'ctrl' => [
            'title' => '[feusers]',
            'label' => 'uid',
            'dividers2tabs' => true,
            'searchFields' => 'first_name,last_name,address,telephone,email,zip,city,company,position,description',
            'hideAtCopy' => true,
            'prependAtCopy' => 'LLL:EXT:lang/locallang_general.xlf:LGL.prependAtCopy',
            'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('imia_user', 'Configuration/Cache/TCA/User.php'),
        ]
    ];
}
// ******************************************************************
// TCA for IMIA\ImiaUser\Domain\Model\UserGroup
// (fe_groups)
// ******************************************************************
if (array_key_exists('fe_groups', $GLOBALS['TCA'])) {
    $GLOBALS['TCA']['fe_groups']['ctrl']['hideAtCopy'] = true;
    $GLOBALS['TCA']['fe_groups']['ctrl']['prependAtCopy'] = 'LLL:EXT:lang/locallang_general.xlf:LGL.prependAtCopy';
    if (array_key_exists('title', $GLOBALS['TCA']['fe_groups']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['fe_groups']['columns']['title'])) {
            $GLOBALS['TCA']['fe_groups']['columns']['title']['config']['max'] = '255';
            $GLOBALS['TCA']['fe_groups']['columns']['title']['config']['type'] = 'input';
        } else {
            $GLOBALS['TCA']['fe_groups']['columns']['title']['config'] = [
                'max' => '255',
                'type' => 'input',
            ];
        }
    } else {
        $tempColumn = [
            'title' => [
                'label' => '[usergroup_title]',
                'config' => [
                    'max' => '255',
                    'eval' => 'trim',
                    'type' => 'input',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_groups', $tempColumn);
    }
} else {
    $GLOBALS['TCA']['fe_groups'] = [
        'ctrl' => [
            'title' => '[fegroups]',
            'label' => 'uid',
            'dividers2tabs' => true,
            'hideAtCopy' => true,
            'prependAtCopy' => 'LLL:EXT:lang/locallang_general.xlf:LGL.prependAtCopy',
            'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('imia_user', 'Configuration/Cache/TCA/UserGroup.php'),
        ]
    ];
}