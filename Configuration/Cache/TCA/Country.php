<?php
if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}

$TCA['static_countries'] = [
    'ctrl' => $TCA['static_countries']['ctrl'],
    'interface' => [
        'showRecordFieldList' => '',
    ],
    'types' => [
        0 => [
            'showitem' => '',
        ],
    ],
    'palettes' => [
    ],
    'columns' => [
   ],
];