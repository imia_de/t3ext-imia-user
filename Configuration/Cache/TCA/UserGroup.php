<?php
if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}

$TCA['fe_groups'] = [
    'ctrl' => $TCA['fe_groups']['ctrl'],
    'interface' => [
        'showRecordFieldList' => 'title',
    ],
    'types' => [
        0 => [
            'showitem' => 'title',
        ],
    ],
    'palettes' => [
    ],
    'columns' => [
        'title' => [
            'label' => '[usergroup_title]',
            'config' => [
                'max' => '255',
                'eval' => 'trim',
                'type' => 'input',
            ],
        ],
   ],
];