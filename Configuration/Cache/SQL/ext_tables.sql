### EXTENSION: imia_user ###

## CLASS: IMIA\ImiaUser\Domain\Model\User
# Table structure for table 'fe_users'
CREATE TABLE `fe_users` (
    `email` varchar(255) DEFAULT '' NOT NULL,
    `country` int(11) DEFAULT '0' NOT NULL,
    `gender` varchar(12) DEFAULT '' NOT NULL,
    `position` varchar(255) DEFAULT '' NOT NULL,
    `mobile` varchar(255) DEFAULT '' NOT NULL,
    `confirmed` tinyint(2) DEFAULT '0' NOT NULL,
    `confirm_token` varchar(32) DEFAULT NULL,
    `activate_token` varchar(32) DEFAULT NULL,
    `language` varchar(32) DEFAULT NULL,
    `pdf` int(11) DEFAULT '0' NOT NULL,
    `description` text NOT NULL,
    `gtc` tinyint(3) DEFAULT '0' NOT NULL,
    `gtc_time` int(11) DEFAULT NULL,
    `register_checks` text NOT NULL
);

## CLASS: IMIA\ImiaUser\Domain\Model\UserGroup
# Table structure for table 'fe_groups'
CREATE TABLE `fe_groups` (
    `title` varchar(255) DEFAULT '' NOT NULL
);

