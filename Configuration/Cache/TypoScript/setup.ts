### EXTENSION: imia_user ###

config.tx_extbase {
    persistence {
        classes {
            IMIA\ImiaUser\Domain\Model\Country {
                mapping {
                    tableName = static_countries
                    columns {
                        cn_iso_2.mapOnProperty = iso2
                        cn_iso_3.mapOnProperty = iso3
                        cn_iso_nr.mapOnProperty = isoNr
                        cn_parent_tr_iso_nr.mapOnProperty = parentTrIsoNr
                        cn_official_name_local.mapOnProperty = officialNameLocal
                        cn_official_name_en.mapOnProperty = officialNameEn
                        cn_capital.mapOnProperty = capital
                        cn_tldomain.mapOnProperty = tldomain
                        cn_currency_iso_3.mapOnProperty = currencyIso3
                        cn_currency_iso_nr.mapOnProperty = currencyIsoNr
                        cn_address_format.mapOnProperty = addressFormat
                        cn_zone_flag.mapOnProperty = zoneFlag
                        cn_short_local.mapOnProperty = shortLocal
                        cn_short_en.mapOnProperty = shortEn
                        cn_short_de.mapOnProperty = shortDe
                        cn_short_fr.mapOnProperty = shortFr
                        cn_short_it.mapOnProperty = shortIt
                        cn_short_cz.mapOnProperty = shortCz
                        cn_short_pl.mapOnProperty = shortPl
                        cn_short_pt.mapOnProperty = shortPt
                        cn_short_ro.mapOnProperty = shortRo
                        cn_short_zh.mapOnProperty = shortZh
                        cn_short_es.mapOnProperty = shortEs
                        cn_short_da.mapOnProperty = shortDa
                        cn_uno_member.mapOnProperty = unoMember
                    }
                }
            }
            IMIA\ImiaUser\Domain\Model\Language {
                mapping {
                    tableName = sys_language
                    columns {
                        static_lang_isocode.mapOnProperty = isocode
                        language_isocode.mapOnProperty = iso
                    }
                }
            }
            IMIA\ImiaUser\Domain\Model\User {
                mapping {
                    tableName = fe_users
                }
            }
            IMIA\ImiaUser\Domain\Model\UserGroup {
                mapping {
                    tableName = fe_groups
                }
            }
        }
    }

}

plugin.tx_imiauser {
    persistence {
        classes {
            IMIA\ImiaUser\Domain\Model\Country {
                mapping {
                    tableName = static_countries
                    columns {
                        cn_iso_2.mapOnProperty = iso2
                        cn_iso_3.mapOnProperty = iso3
                        cn_iso_nr.mapOnProperty = isoNr
                        cn_parent_tr_iso_nr.mapOnProperty = parentTrIsoNr
                        cn_official_name_local.mapOnProperty = officialNameLocal
                        cn_official_name_en.mapOnProperty = officialNameEn
                        cn_capital.mapOnProperty = capital
                        cn_tldomain.mapOnProperty = tldomain
                        cn_currency_iso_3.mapOnProperty = currencyIso3
                        cn_currency_iso_nr.mapOnProperty = currencyIsoNr
                        cn_address_format.mapOnProperty = addressFormat
                        cn_zone_flag.mapOnProperty = zoneFlag
                        cn_short_local.mapOnProperty = shortLocal
                        cn_short_en.mapOnProperty = shortEn
                        cn_short_de.mapOnProperty = shortDe
                        cn_short_fr.mapOnProperty = shortFr
                        cn_short_it.mapOnProperty = shortIt
                        cn_short_cz.mapOnProperty = shortCz
                        cn_short_pl.mapOnProperty = shortPl
                        cn_short_pt.mapOnProperty = shortPt
                        cn_short_ro.mapOnProperty = shortRo
                        cn_short_zh.mapOnProperty = shortZh
                        cn_short_es.mapOnProperty = shortEs
                        cn_short_da.mapOnProperty = shortDa
                        cn_uno_member.mapOnProperty = unoMember
                    }
                }
            }
            IMIA\ImiaUser\Domain\Model\Language {
                mapping {
                    tableName = sys_language
                    columns {
                        static_lang_isocode.mapOnProperty = isocode
                        language_isocode.mapOnProperty = iso
                    }
                }
            }
            IMIA\ImiaUser\Domain\Model\User {
                mapping {
                    tableName = fe_users
                }
            }
            IMIA\ImiaUser\Domain\Model\UserGroup {
                mapping {
                    tableName = fe_groups
                }
            }
        }
    }

}

module.tx_imiauser {
    persistence {
        classes {
            IMIA\ImiaUser\Domain\Model\Country {
                mapping {
                    tableName = static_countries
                    columns {
                        cn_iso_2.mapOnProperty = iso2
                        cn_iso_3.mapOnProperty = iso3
                        cn_iso_nr.mapOnProperty = isoNr
                        cn_parent_tr_iso_nr.mapOnProperty = parentTrIsoNr
                        cn_official_name_local.mapOnProperty = officialNameLocal
                        cn_official_name_en.mapOnProperty = officialNameEn
                        cn_capital.mapOnProperty = capital
                        cn_tldomain.mapOnProperty = tldomain
                        cn_currency_iso_3.mapOnProperty = currencyIso3
                        cn_currency_iso_nr.mapOnProperty = currencyIsoNr
                        cn_address_format.mapOnProperty = addressFormat
                        cn_zone_flag.mapOnProperty = zoneFlag
                        cn_short_local.mapOnProperty = shortLocal
                        cn_short_en.mapOnProperty = shortEn
                        cn_short_de.mapOnProperty = shortDe
                        cn_short_fr.mapOnProperty = shortFr
                        cn_short_it.mapOnProperty = shortIt
                        cn_short_cz.mapOnProperty = shortCz
                        cn_short_pl.mapOnProperty = shortPl
                        cn_short_pt.mapOnProperty = shortPt
                        cn_short_ro.mapOnProperty = shortRo
                        cn_short_zh.mapOnProperty = shortZh
                        cn_short_es.mapOnProperty = shortEs
                        cn_short_da.mapOnProperty = shortDa
                        cn_uno_member.mapOnProperty = unoMember
                    }
                }
            }
            IMIA\ImiaUser\Domain\Model\Language {
                mapping {
                    tableName = sys_language
                    columns {
                        static_lang_isocode.mapOnProperty = isocode
                        language_isocode.mapOnProperty = iso
                    }
                }
            }
            IMIA\ImiaUser\Domain\Model\User {
                mapping {
                    tableName = fe_users
                }
            }
            IMIA\ImiaUser\Domain\Model\UserGroup {
                mapping {
                    tableName = fe_groups
                }
            }
        }
    }

}

