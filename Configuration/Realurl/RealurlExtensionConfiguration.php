<?php
foreach (['user_access', 'user_profile', 'user_register', 'user_delete'] as $plugin) {
    $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['preVars'][] = [
        'GETvar'  => 'tx_imiauser_' . $plugin . '[controller]',
        'noMatch' => 'bypass',
    ];
}

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['postVarSets']['_DEFAULT']['register'] = [
    [
        'GETvar'   => 'tx_imiauser_user_register[action]',
        'valueMap' => [
            'submit'           => 'registerSubmit',
            'success'          => 'registerSuccess',
            'confirm'          => 'confirm',
            'confirm-success'  => 'confirmSuccess',
            'activate'         => 'activate',
            'activate-success' => 'activateSuccess',
        ],
        'noMatch'  => 'bypass',
    ],
    [
        'GETvar' => 'tx_imiauser_user_register[token]',
    ],
];

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['postVarSets']['_DEFAULT']['profile'] = [
    [
        'GETvar'   => 'tx_imiauser_user_profile[action]',
        'valueMap' => [
            'delete'         => 'delete',
            'delete-success' => 'deleteSuccess',
        ],
        'noMatch'  => 'bypass',
    ],
];

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['postVarSets']['_DEFAULT']['access'] = [
    [
        'GETvar'   => 'tx_imiauser_user_access[action]',
        'valueMap' => [
            'delete'         => 'delete',
            'delete-success' => 'deleteSuccess',
        ],
        'noMatch'  => 'bypass',
    ],
];

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['postVarSets']['_DEFAULT']['delete'] = [
    [
        'GETvar'   => 'tx_imiauser_user_delete[action]',
        'valueMap' => [
            'delete-success' => 'deleteSuccess',
        ],
        'noMatch'  => 'bypass',
    ],
];